<?php


	//////////////////////////////////////////////
	// Includes BDD
	//////////////////////////////////////////////
	
	include_once '../includes/functions.php'; 
	
	//////////////////////
	// Démarrage session
	//////////////////////
	
	sec_session_start();
	
	//////////////////////
	// Infos de temps
	//////////////////////
	
	include_once '../includes/time_stuff.php';
	
	/////////////////////////////////////
	// Variables globales
	/////////////////////////////////////

	$user_id=$_SESSION['user_id'];
	$uid=intval($user_id); // identifiant joueur
	
	$error_msg="";
	$to = 'psykhaze@hotmail.fr'; 
	$player=1744;
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$list_stmt = $mysqli->prepare("SELECT `token` FROM `player_data` WHERE `ID_Player_Profile` = ?"); 
	$list_stmt->bind_param('i',$player);
	$list_stmt->bind_result($bddtoken);
	$list_stmt->execute();
	$list_stmt->fetch();
	$token=$bddtoken;
	$list_stmt->store_result();
	$list_stmt->close();

	$link="http://.fr/index.php?menuid=4&id=".$uid."&token=".$token;
	$subject = 'R&eacute;initialisation mot de passe de votre Compte - Green Hops';

	$message = '<div style="min-width:70%;margin:40px;border-radius:1em;font-family:Helvetica;font-size:1.5em;color:#fff;" align="left">';
	$message = '<H1 style="font-family:Helvetica;font-size:1.5em;color:#006e2e;">R&eacute;activation de votre compte Green Hops</H1>';
	$message.= 'Merci de cliquer sur le lien suivant :<br><br>';
	$message.= '<p><a href="'.$link.'\"> Lien</a></p></div>';
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: greenhopslejeu@gmail.com' . "\r\n" .
	'Reply-To: greenhopslejeu@gmail.com' . "\r\n" .
	'X-Mailer: PHP/' . phpversion(); 
	mail($to, $subject, $message, $headers);
?>