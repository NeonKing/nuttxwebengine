<?php



/////////////////////////////////////////
// 1 Fonction Green Hops : Récupération de l'inventaire d'un joueur - renvoies un tableau
/////////////////////////////////////////

function get_player_inventory ($uid)
{
	$tmp = client_query_db ($_SESSION['mysqli'], 
	"SELECT  `Inventory` 
		FROM `gh.character_specs` 
		WHERE `ID_Character` = $uid");
	$inventory = explode('-', $tmp[0][0]);
	return $inventory;
}

/////////////////////////////////////////
// 2 Fonction Green Hops : Récupération du stock houblon d'un joueur - renvoies un tableau
/////////////////////////////////////////

function get_hop_stock($uid)
{
	$tmp = client_query_db ($_SESSION['mysqli'], 
	"SELECT  `Houblonstock` 
		FROM `gh.character_specs` 
		WHERE `ID_Character` = $uid");
	$hop_stock = explode('-', $tmp[0][0]);
	return $hop_stock;
}
/////////////////////////////////////////
// 3 Fonction Green Hops : MAJ du stock houblon d'un joueur - renvoies un booléen
/////////////////////////////////////////

function update_hop_qty($uid, $hopid, $qty)
{
	//Récupération tableau d'inventaire
	$hop_stock=get_hop_stock($uid); // Tableau X éléments représentant les quantités des élements avec index=id
	$index=$hopid;//base indexage tableau=0
	if (($hop_stock[$index]+$qty)>=0) //Nanan pas d'ardoise,le stock reste positif
	{
		$hop_stock[$index]+=$qty;
	}
	$updatestr="";
	for ($i=0;$i<4;$i++)
	{
		$updatestr.=$hop_stock[$i]."-";
	}
	$updatestr.=$hop_stock[4];
	
	if (client_query_db ($_SESSION['mysqli'], 
	"UPDATE `gh.character_specs` 
		SET `Houblonstock`= '$updatestr' 
		WHERE `ID_Character`= $uid"))
	{
		return false;
	}
	else
	{
		return true;	
	}
	
}

/////////////////////////////////////
// 4 Fonction green hops - renvoies une chaine contenant le nom de la marque selon l'ID
/////////////////////////////////////

function get_trend_name($trend_id)
{
	$tmp = client_query_db ($_SESSION['mysqli'], 
	"SELECT `Name` 
		FROM `gh.trendz` 
		WHERE `ID`= $trend_id");
	return $tmp[0][0];
}

/////////////////////////////////////
// 5 Fonction green hops : retourne liste des ids, departements et noms de la totalité des shops
/////////////////////////////////////

function list_select_shops()
{
	$tmp = client_query_db ($_SESSION['mysqli'], 
	"SELECT `ID`,`Dpt`,`Name` 
		FROM `gh.shops` 
		ORDER BY `Dpt` 
		ASC");
	return $tmp;
}

/////////////////////////////////////
// 6 Fonction Environnement green hops : Récupération de l'état d'équipement d'une room - renvoies un tableau d'index d'items
/////////////////////////////////////

function get_room_equipement($room_number,$uid)
{
	if ($room_number > 0 && $room_number < 17)
	{
		$tmp = client_query_db($_SESSION['mysqli'], 
		"SELECT `Room_Equip$room_number` 
			FROM `gh.env_specs` 
			WHERE `ID_Owner` = $uid");
		$equip = explode('-', $tmp[0][0]);
		return $equip;
	}
	else
	{
		return '';
	}
}

/////////////////////////////////////
// 7 Fonction Environnement : MAJ de l'état d'équipement d'une room - renvoies un booléen
/////////////////////////////////////

function update_room_equipement($newequip, $room_number,$uid)
{
	if ($room_number > 0 && $room_number < 17)
	{
		if (client_query_db($_SESSION['mysqli'], 
		"UPDATE `gh.env_specs` 
			SET `Room_Equip$room_number` = '$newequip' 
			WHERE `ID_Owner` = $uid") == 0)
		{
			return true;
		}
	}
	return false;

}

/////////////////////////////////////////
// 8 Fonction Environnement : Récupération de l'état global de room - renvoies un tableau
/////////////////////////////////////////

function get_room_state($room_number,$uid)
{
	if ($room_number > 0 && $room_number < 17)
	{
		$tmp = client_query_db($_SESSION['mysqli'], 
		"SELECT `Room_State$room_number` 
			FROM `gh.env_specs` 
			WHERE `ID_Owner` = $uid");
		$state = explode('-', $tmp[0][0]);
		return $state;
	}
	return '';
}

/////////////////////////////////////////
// 9 Fonction Environnement : Est-ce que la room est accessible pour ce joueur?
/////////////////////////////////////////

function is_room_accessible($room_number,$uid)
{
	if ($room_number > 0 && $room_number < 17)
	{
		$state = "SELECT `Room_State$room_number` 
					FROM `gh.env_specs` 
					WHERE `ID_Owner` = $uid";
		$substring = "SELECT SUBSTRING_INDEX(($state), '-', 3)";
		$request = "SELECT SUBSTRING(($substring) FROM -1)";
		$tmp = client_query_db($_SESSION['mysqli'], $request);
		if ($tmp[0][0] != 0)
			return true;
	}
	return false;
}

/////////////////////////////////////
// 10 Fonction Environnement : Mise à Jour de l'état d'une room - renvoies true ou false
/////////////////////////////////////

function update_room_state($newstate, $room_number, $uid)
{
	if ($room_number > 0 && $room_number < 17)
	{
		if (client_query_db($_SESSION['mysqli'], 
		"UPDATE `gh.env_specs` 
			SET `Room_State$room_number` = '$newstate' 
			WHERE `ID_Owner` = $uid") == 0)
		{
			return true;	
		}
	}
	return false;	
}

/////////////////////////////////////////
// 11 Fonction Environnement : Récupération de l'état global de la brasserie - renvoies un tableau
/////////////////////////////////////////

function get_brewery_state($uid)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `Brewery_State` 
		FROM `gh.env_specs` 
		WHERE `ID_Owner` = $uid	");
	$state = explode('-', $tmp[0][0]);
	return $state;
}

/////////////////////////////////////////
// 12 Fonction Environnement : MAJ de l'état global de la brasserie 
/////////////////////////////////////////

function update_brewery_state($newstate,$uid)
{
	if (client_query_db($_SESSION['mysqli'], 
	"UPDATE `gh.env_specs` 
		SET `Brewery_State` = '$newstate' 
		WHERE `ID_Owner` = $uid") == 0)
	{
		return true;
	}
}

/////////////////////////////////////////
// 13 Fonction Environnement : Récupération d'un état slot brasserie - renvoies un tableau
/////////////////////////////////////////

function get_brewery_slot_state($slotnb,$uid)
{
	if ($slotnb > 0 && $slotnb < 6)
	{
		$tmp = client_query_db($_SESSION['mysqli'], 
		"SELECT `Brewery_Slot_State$slotnb`, `Brewery_Slot_Val$slotnb` 
			FROM `gh.env_specs` 
			WHERE `ID_Owner` = $uid");
		$state = explode('-', $tmp[0][0]);
		$state[] = $tmp[0][1];
		return $state;
	}
	return '';
}

/////////////////////////////////////////
// 14 Fonction Environnement : MAJ d'un état slot brasserie 
/////////////////////////////////////////

function update_brewery_slot_state($newbrewstate,$qty,$slotnb,$uid)
{
	if ($slotnb > 0 && $slotnb < 6)
	{
		if (client_query_db($_SESSION['mysqli'], 
		"UPDATE `gh.env_specs` 
			SET `Brewery_Slot_State$slotnb` = '$newbrewstate', `Brewery_Slot_Val$slotnb` = $qty 
			WHERE `ID_Owner` = $uid") == 0)
			return true;
	}
	return false;
}

/////////////////////////////////////////
// 15 Fonction Environnement : Récupération d'un état slot potes - renvoies un tableau
/////////////////////////////////////////

function get_friends_slot_state($slotnb,$uid)
{
	if ($slotnb > 0 && $slotnb < 5)
	{
		$tmp = client_query_db($_SESSION['mysqli'], 
		"SELECT `Friend_Slot_State$slotnb` 
			FROM `gh.env_specs` 
			WHERE `ID_Owner` = $uid");
		$state = explode('-', $tmp[0][0]);
		return $state;
	}
	return '';
}

/////////////////////////////////////////
// 16 Fonction Environnement : MAJ d'un état slot potes
/////////////////////////////////////////

function update_friends_slot_state($newslotstate,$slotnb,$uid)
{
	if ($slotnb > 0 && $slotnb < 5)
	{
		if (client_query_db($_SESSION['mysqli'], 
		"UPDATE `gh.env_specs` 
			SET `Friend_Slot_State$slotnb` = '$newslotstate' 
			WHERE `ID_Owner` = $uid", $error) == 0)
		{
			return true;
		}
	}
	return false;
}

/////////////////////////////////////
// 17 Fonction Environnement : Quizz : Recupere les questions de la marque donnee en parametre
/////////////////////////////////////

function select_questions_from_trendz($id_trend, $lang, $nb_questions = 5)
{
	$qas = client_query_db($_SESSION['mysqli'],
	"SELECT `I18n_Question_Label_ID`, `True_Answer_ID`, `Fake_answer_1_ID`, `Fake_answer_2_ID`, `Fake_answer_3_ID`, `Fake_answer_4_ID` 
		FROM `plugin.questions` 
		WHERE `ID_Quizz` 
		IN (SELECT `ID` 
			FROM `plugin.quizz` 
			WHERE `ID_Trend` = $id_trend)");
	shuffle($qas);
	$qas = array_slice($qas, 0, $nb_questions);
	for ($i = 0; $i < $nb_questions  ; $i++)
	{		
		$answers = array("r" => $qas[$i][1], 
							"f1" => $qas[$i][2], 
							"f2" => $qas[$i][3], 
							"f3" => $qas[$i][4], 
							"f4" => $qas[$i][5]);
		foreach ($answers as $key => $value)
		{
			if ((intval($value))!=0)
			{
				$answers[$key] = get_translated_label(($value+106), $lang);
			}
		}	
		$ret[$i] = array(get_translated_label(($qas[$i][0]), $lang), $answers);
	}
	return $ret;
}

/////////////////////////////////////
// 18 Fonction green hops : liste un certain nombre de joueurs,classés par niveau,points et crédit
/////////////////////////////////////

function list_ranking($limit = 100)
{
	$ranking = client_query_db ($_SESSION['mysqli'],
	"SELECT pp.`ID`, pp.`Username`, cs.`points`
		FROM `gh.character_specs` cs 
		JOIN `pd.player_profile` pp 
		ON cs.`ID_Character` = pp.`ID`
		ORDER BY cs.`points` 
		DESC 
		LIMIT $limit");
	$players = array();
	$players = array_pad($players, $limit, array());
	for ($i = 0; $i < count($ranking); $i++)
	{
		$players[$i][] = $ranking[$i][0];
		$players[$i][] = get_numeric_player_characteristic('Niveau',$ranking[$i][0]);
		$players[$i][] = $ranking[$i][2];
		$players[$i][] = get_numeric_player_characteristic('credit',$ranking[$i][0]);
		$players[$i][] = $ranking[$i][1];
	//	$players[$i][] = get_char_avatar_id($ranking[$i][0]);
	}

	return $players;
}

/////////////////////////////////////
// 19 Fonction green hops : Renvoie les pseudos des joueurs ayant atteint les niveaux 3 a 16 en premier
/////////////////////////////////////

function get_lvl_ranking($lvlmin = 3, $lvlmax = 16)
{
	for ($lvl = $lvlmin; $lvl <= $lvlmax; $lvl++)
	{
		$tmp = client_query_db ($_SESSION['mysqli'], 
		"SELECT pp.`ID`, sl.`Moment`, pp.`Username` 
			FROM `logger.succes_log` sl 
			JOIN `pd.player_profile` pp 
			ON sl.`ID_Character`=pp.`ID` 
			WHERE `ID_Success` = 1 AND `amount` = $lvl");
		$timestamp = INF;
		for ($i = 0; $i < count($tmp); $i++)
		{
			if ($tmp[$i][1] < $timestamp)
			{
				$timestamp = $tmp[$i][1];
				$ret[$lvl - $lvlmin] = array('ID' => $tmp[$i][0], 
												'Username' => $tmp[$i][2]);
			}
		}
		if (!isset($ret[$lvl - $lvlmin]))
		{
			$ret[$lvl - $lvlmin] = array('ID' => 0, 
											'Username' => 'MissingNo');	
		}
	}
	return $ret;
}
/////////////////////////////////////
// 20 Fonction greenhops : Renvoie le classement et les points du joueur ainsi que des 10 precedents et suivants
/////////////////////////////////////

function get_player_rank($uid, $surround = 10)
{
	$tmp = client_query_db ($_SESSION['mysqli'], 
	"SELECT cp.`points`, pp.`Username`, pp.`ID` 
		FROM `gh.character_specs` cp 
		LEFT JOIN `pd.player_profile` pp 
		ON cp.`ID_Owner`=pp.`ID` 
		ORDER BY cp.`points` 
		DESC");
	$found = false;
	$stop_counter = 0;
	for ($i = 0; $i < count($tmp); $i++)
	{
		if ($stop_counter > $surround)
			break;
			
		$ret[] = array($tmp[$i][0], 
						$tmp[$i][1], 
						$tmp[$i][2]);
		if ($tmp[$i][2] == $uid)
			$found = true;
		if ($found)
			$stop_counter++;
	}
	$ret = array_slice($ret, - (2 * $surround + 1));
	return $ret;
}

/////////////////////////////////////
// 21 Fonction greenhops : Récupération de la valeur numérique d'une caractéristique joueur - renvoies un nombre
/////////////////////////////////////

function get_numeric_player_characteristic($characteristic,$uid)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `$characteristic` 
		FROM `gh.character_specs` 
		WHERE `ID_Owner` = $uid");
	return $tmp[0][0];
}

/////////////////////////////////////
// 22 Fonction greenhops : MAJ de la valeur numérique d'une caractéristique joueur - renvoies true si tout se passe bien, false sinon
/////////////////////////////////////

function update_numeric_player_characteristic($value_to_add,$characteristic, $uid)
{
	$chars_ids = 
	"SELECT `Mapping` 
		FROM `gh.fields_mapping` 
		WHERE `ID` = 1";
	$request = 
	"SELECT cs.`$characteristic`, cst.`Min_Val`, cst.`Max_Val`
		FROM `gh.character_specs_types` cst, `gh.character_specs` cs
		WHERE `ID_Owner` = $uid 
		AND cst.`ID` = FIND_IN_SET('$characteristic', ($chars_ids))";
	$tmp = client_query_db($_SESSION['mysqli'], $request);
	$new_val = max(min($tmp[0][0] + $value_to_add, $tmp[0][2]), $tmp[0][1]);
	if (client_query_db($_SESSION['mysqli'], 
	"UPDATE `gh.character_specs` 
		SET `$characteristic` = $new_val 
		WHERE `ID_Character` = $uid") == 0)
	{
		return true;	
	}
	return false;
}

/////////////////////////////////////
// 23 Fonction Environnement : renvoie le climat de la room
/////////////////////////////////////

function get_room_climat($room, $uid)
{
	$equip = get_room_equipement($room, $uid);
	$state = get_room_state($room, $uid);
	$heat = 0;
	$extraction = 0;
	$lampIndex = 0;
	$ballastIndex = 1;
	$reflectorIndex = 2;
	$extractorIndex = 3;
	$coatingIndex = 6;

	for ($i = 0; $i < $state[2]; $i++)
	{
		$tmp = 0;
		if ($equip[$lampIndex + $i * 10] != 0)
		{
			$tmp = get_item_numeric_property($equip[$lampIndex + $i * 10], 'Puissance');
			if ($equip[$ballastIndex + $i * 10] != 0)
				$tmp *= get_item_numeric_property($equip[$ballastIndex + $i * 10], 'Chaleur');
			if ($equip[$reflectorIndex + $i * 10] != 0)
				$tmp *= get_item_numeric_property($equip[$reflectorIndex + $i * 10], 'Chaleur');
			if ($equip[$coatingIndex + $i * 10] != 0)
				$tmp *= get_item_numeric_property($equip[$coatingIndex + $i * 10], 'Chaleur');
		}
		$heat += $tmp;
		$extraction += get_item_numeric_property($equip[$extractorIndex + $i * 10], 'M3h');
	}
	
	if ($heat != 0)
	{
		$coef = min(1, $extraction / $heat);
		return ($coef < 0.1)?0.1:$coef;
	}
	else
		return 0.1;
}

/////////////////////////////////////
// 24 Fonction Environnement : renvoie les lumens d'un slot de room
/////////////////////////////////////

function get_room_slot_lumens($room, $slot, $uid)
{
	$equip = get_room_equipement($room, $uid);
	$lampIndex = 0;
	$ballastIndex = 1;
	$reflectorIndex = 2;
	$coatingIndex = 6;

	$lumens = get_item_numeric_property($equip[$lampIndex + $slot * 10], 'Puissance')
		* get_item_numeric_property($equip[$ballastIndex + $slot * 10], 'Lumens')
		* get_item_numeric_property($equip[$reflectorIndex + $slot * 10], 'Lumens');
	if ($equip[$coatingIndex + $slot * 10] != 0)
		$lumens *= get_item_numeric_property($equip[$coatingIndex + $slot * 10], 'Lumens');
	
	return $lumens;
}

/////////////////////////////////////
// 25 Fonction Environnement : Calcul et maj le rendememt d'une room
/////////////////////////////////////

function update_rendement($roomnb, $nb_steps, $uid)
{
	$statet = get_room_state($roomnb,$uid);
	$rstate= $statet[0];
	$upto= $statet[1];
	$upgrade = $statet[2];
	$dirt=$statet[3];
	$taille = $statet[4];
	$palissage = $statet[5];
	$rendement = explode(':', $statet[6]);
	$stepsdone = $statet[7];
	$equip = get_room_equipement($roomnb,$uid);
	
	$error = false;
	$seedMinValue = 11;
	$seedIndex = 7;
	$fertilizerIndex = 8;
	// Pour chaque step a realiser
	for ($k = 0; $k < $nb_steps; $k++)
	{
		$toAdd = [0, 0, 0, 0, 0];
		$climat = get_room_climat($roomnb, $uid);
		// Pour chaque slot de graine
		for ($i = 0; $i < $upgrade; $i++)
		{
			// Si le slot est rempli, on calcul le rendement pour celui-ci
			if ($equip[$seedIndex + $i * 10] != 0)
			{
				//TODO ameliorer le calcul de la maladie
				$illness = max((10 - $dirt) / 10, 0.01);
				$slotLumens = get_room_slot_lumens($roomnb, $i, $uid);
				$engrais = ($equip[$fertilizerIndex + $i * 10] != 0)?get_item_numeric_property($equip[$fertilizerIndex + $i * 10], 'Coef'):1;
				$stepRendement = $slotLumens * $climat * $taille * $palissage * $engrais * $illness;
				$toAdd[$equip[$seedIndex + $i * 10] - $seedMinValue] += $stepRendement;
			}
		}
		// On met a jour le rendement de chaque variete pour ce step
		for ($i = 0; $i < count($toAdd); $i++)
			if ($toAdd[$i] != 0)
				//$rendement[$equip[$seedIndex + $i * 10] - $seedMinValue] = floor(($rendement[$equip[$seedIndex + $i * 10] - $seedMinValue] * $stepsdone + $toAdd[$equip[$seedIndex + $i * 10] - $seedMinValue]) / ($stepsdone + 1));
				$rendement[$i] = floor(($rendement[$i] * $stepsdone + $toAdd[$i]) / ($stepsdone + 1));
		$stepsdone++;
		$newequip = $rstate."-".$upto."-".$upgrade."-".$dirt."-".$taille."-".$palissage."-".implode(':', $rendement)."-".$stepsdone;
		if (!update_room_state($newequip, $roomnb,$uid))
			$error = true;
	}
	return ($error)?false:true;
}

function equip_room($roomNb,$postVars,$uid)
{
	$roomState = get_room_state($roomNb,$uid);
	$roomUpgrade = $roomState[2];
	$roomEquip = get_room_equipement($roomNb,$uid);
	$level = get_numeric_player_characteristic('Niveau',$uid);
	$catnumber=10;

	for ($l=0;$l<$roomUpgrade;$l++)
	{
		$offset = $l * $catnumber;
		$indexcat = 0;
		$endslot = $offset + $catnumber -1;
		for ($j=$offset;$j<$endslot;$j++)
		{
			$indexcat++;
			$varname=$l."-cat".$indexcat;
			if ($roomEquip[$j] != $postVars[$varname])
			{
				$properties = get_item_properties($postVars[$varname]);
				if ($properties['Niveau'] <= $level && $properties['Niveau'] <= $roomNb)
				{
					if (update_item_qty($uid, $postVars[$varname], -1))
					{
						if ($roomEquip[$j] != 0)
						{
							update_item_qty($uid, $roomEquip[$j], 1);
						}
						$roomEquip[$j]=$postVars[$varname];
					}
				}				
				else
				{
					$roomEquip[$j]='0';
				}
			}
		}
	}
	$finalEquip = '';
	for ($k=0;$k<49;$k++)
	{
		$finalEquip.=$roomEquip[$k]."-";
	}
	$finalEquip.="0";
	if (!update_room_equipement($finalEquip,$roomNb,$uid))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function get_pnj_text($Env,$Spec=1)
{
 	$tmp= client_query_db($_SESSION['mysqli'],
	"SELECT hlp.`I18n_Label_ID`, itt.`ID_I18n_Regroup`, itt.`T_Val`
		FROM `i18n.helpers` hlp 
		JOIN `i18n.translation_text` itt 
		ON hlp.`I18n_Label_ID` = itt.`ID_I18n_Regroup`
		WHERE hlp.`ID_Parent_Env` = $Env 
		AND hlp.`ID` = $Spec
		AND hlp.`ID_Lang` = ".$_SESSION['lang']);
	return mb_encode_numericentity($tmp[0][2], array(0x80, 0xff, 0, 0xff));
}

/*
// Récupération des paramètres d'équipement liés à la culture

function get_grow_params($RoomNb,$Slot,$uid)
{
	$equip = get_room_equipement($RoomNb, $uid);
	// Offset de slot
	$EquipOffset=($Slot-1)*$ItemsByRoom;
	// Index relatif des équipemets 
	$IndexBallast= $EquipOffset+1;
	$IndexReflector= $EquipOffset+2;
	$IndexExtractor= $EquipOffset+3;
	$IndexCoating = $EquipOffset+6;
	$IndexSeed = $EquipOffset+7;
	$IndexFertilizer = $EquipOffset+8;
	// Renvoi des propriétés numériques adéquates pour le calcul sous forme de tableau
	return array(0=>get_item_numeric_property($equip[$EquipOffset],'Puissance'),
					get_item_numeric_property($equip[$IndexBallast],'Chaleur'),
					get_item_numeric_property($equip[$IndexBallast], 'Lumens'),
					get_item_numeric_property($equip[$IndexReflector],'Chaleur'),
					get_item_numeric_property($equip[$IndexReflector],'Lumens'),
					get_item_numeric_property($equip[$IndexExtractor],'M3h'),
					get_item_numeric_property($equip[$IndexCoating],'Chaleur'),
					get_item_numeric_property($equip[$IndexCoating],'Lumens') );
}

//Génration de la base houblon

function generate_base_hop($RoomNb,$Slot,$uid)
{
	$equip = get_room_equipement($RoomNb, $uid);
	$Expertise=get_numeric_player_characteristic('science',$uid);
	$SlotPower=get_item_numeric_property($equip[($Slot-1)*$ItemsByRoom],'Puissance'); // Récupération de la puissance relative au slot
	$Variation=1+((rand(1,$PercentVariation)+$Expertise)/100); // Facteur de variation aléatoire relatif à l'expertise'
	return floor( ($SlotPower*$PowerBase[$SlotPower]*$Variation) / 10 ); // Calcul de la base houblon
}
	
// Fonction step de récolte négatif
		
function make_grow_problem($Problem,$Base)
{
	
	// Traitement du problème selon sa qualité : Attribution d'un facteur de réduction'
	switch ($Problem)
	{
		case 1: //'overheat'
		case 2: // 'illness'
			$rate=1.5-(rand(0,$PercentVariation)/100); // Réduction du rendement
			break;
		case 3: //'fire'
			$rate=0; // En cas de feu, destruction des installations du slot
			// Déséquiper le slot
			break;
	}
	// retour valeur modifiée 
	return floor($Base*$rate);
}

// Fonction step de récolte positif

function make_grow_increase($RoomNb,$Slot,$Base)
{
	// Paramètres de base
	$Expertise = get_numeric_player_characteristic('science',$uid);
	$Params = get_grow_params($RoomNb,$Slot,$uid);
	// Calcul des facteurs interférentiels
	$ClimaticRate = 1 + (($Params[0]-$Params[5])/100) ; //Facteur climatique
	$LuminosityRate = $Params[4] * $Params[7] ; // FActeur de réflexion d'illumination'
	$ExpertRate = 1 + ($Expertise/100) ; // Facteur Expertise
	// Retour de la valeur augmentée
	return floor($Base*$ClimaticRate*$LuminosityRate*$ExpertRate);
}

// Vérification conditions minimales de plantation d'une room

function check_grow_possible($RoomNb,$Slot,$uid)
{
	$equip = get_room_equipement($RoomNb, $uid);
	// Offset de slot
	$EquipOffset=($Slot-1)*$ItemsByRoom;
	$IndexBallast= $EquipOffset+1;
	$IndexSeed = $EquipOffset+7;
	
	if  ( 	(($equip[$EquipOffset]>=16 ) && ($equip[$EquipOffset]<=19)) &&
			(($equip[$IndexBallast]>=1 ) && ($equip[$IndexBallast]<=2)) &&
			(($equip[$IndexSeed]>=11 ) && ($equip[$IndexSeed]<=15)) )
	{
		return true;			
	}
	else
	{
		return false;
	}
}

// Gestion Processus culture

function update_grow_process($RoomNb,$uid) 
{
	$Expertise = get_numeric_player_characteristic('science',$uid);
	$RoomState = get_room_state($RoomNb,$uid);
	$Params = get_grow_params($RoomNb,1,$uid);
	$RoomCurrentState = $RoomState[0];
	$RoomUpto = $RoomState[1];
	$RoomUpgrade = $RoomState[2];
	$RoomDirt = $RoomState[3];
	$Rendement =  explode(':',$RoomState[4]);
	$RoomVarieties = explode(':',$RoomState[5]);

	switch ($RoomCurrentState) // Traitement selon l'état, MAJ de la chaine d'état de room
	{
		case 'grow':
		case 'bloom' : // Taille ou palissage
			for ($j=0;$j<$RoomUpgrade;$j++) // Pour chaque slot
			{
				$NewRendement[$j]=floor($Rendement[$j]*($PercentTreatment/100)); // On augmente la prod
			}
			$NewRoomState=$RoomCurrentState;
			$NewRoomUpto=$RoomUpto;
			break;
		case 'inactive': // Plantation
			for ($j=0;$j<$RoomUpgrade;$j++) // Pour chaque slot
			{
				$seed = intval(filter_input(INPUT_POST, $j.'-seed',  FILTER_SANITIZE_NUMBER_INT));
				$fertilizer = intval(filter_input(INPUT_POST, $j.'-fertilizer',  FILTER_SANITIZE_NUMBER_INT));
				if ($seed>0)
				{
					$RoomVarieties[$j]=$seed;
					//Equiper la graine et l'engrais & soustraire stock
					if (check_grow_possible($RoomNb,($j+1),$uid))
					{
						$NewRendement[$j] = generate_base_hop($RoomNb,$j,$uid); // Générer la base houblon
						$NewRoomUpto=$RoomUpto+get_action_time(1);
						$NewRoomState='grow';
					}		
				}
			}
			// On verifie avoir suffisament de graines dans l'inventaire pour Soustraire graines et engrais sélectionnées du stock et les équiper
			break;
		case 'growend': // Passage flo
			$Steps=$GrowSteps;
			$NewRoomUpto=$RoomUpto+get_action_time(4);
			$NewRoomState='bloom';
		case 'bloomend': // Récolte
			if (!(isset($Steps)))
			{
				$Steps=$BloomSteps;
				$NewRoomUpto=$now;
				$NewRoomState='inactive';
				$RoomDirt = $RoomDirt + 1; // MAJ saleté
				$availableStock = get_numeric_player_characteristic('StockLvl',$uid) * $StockCapacityPerLevel - array_sum(get_hop_stock($uid));
			}
			$points=0;
			// Générer les pas et évènements de culture
			for ($i=0;$i<$Steps;$i++)
			{
				for ($j=0;$j<$RoomUpgrade;$j++) // Pour chaque slot
				{
					if (rand(1,10)<floor(($PercentUnluck-$Expertise)/200)) // En fonction d'un facteur chance pondéré par l'expertise on augmente la prod ou on gènère un souci
					{	
						$NewRendement[$j]=make_grow_problem(rand(1,3),$Rendement[$j]);
						// Log succes journal
					}
					else
					{
						$NewRendement[$j]=make_grow_increase($RoomNb,($j+1),$Rendement[$j]);
						// Log succès journal
					}
					if (($NewRendement[$j]>0)&&($i==$Steps)&&($RoomCurrentState=='bloomend')) // Si l'on récolte, tous les slots dont rendement>0 = 1 point
					{
						// Si il a plus de houblon recolte que de place dans l'inventaire ,plafond
						// MAJ stock houblon
						// On desequipe les graines et les engrais
						$points++;
					}
				}
			}
			if ($points>0) // Gestion niveau
			{
				update_numeric_player_characteristic($points,'points',$uid);
				if (get_numeric_player_characteristic('points',$uid)>$levelsteps[$level]) // Si le nouveau nombre de points est supérieur au pallier du niveau
				{
					update_numeric_player_characteristic(1,'Niveau',$uid); //On augmentes le niveau
					//log_player_success($uid, , 1, "", get_numeric_player_characteristic('Niveau',$uid));	// Enregistrer le succes + le niveau atteint.
				}
			}
	}
	
	//MAJ état room
	$NewRoomState=$NewRoomState.'-'.$NewRoomUpto.'-'.$RoomDirt.'-'.$RoomUpgrade.'-'.implode(':',$NewRendement).'-'.implode(':',$RoomVarieties);
	// Log evolution
	if (!update_room_state($newroomstate, $roomnb,$uid))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function reset_room($RoomNb,$uid)
{
	$RoomState = get_room_state($RoomNb,$uid);
	$RoomUpgrade = $RoomState[2];
	$RoomDirt = $RoomState[3];
	$newroomstate="inactive-".$now."-".$RoomUpgrade."-".$RoomDirt.'-0:0:0:0:0-0:0:0:0:0'; // Nouvel état de room // Pattern base
	update_room_state($newroomstate, $RoomNb ,$uid);
}
*/
?>