<?php
	function read_folder($folderName, $resultFile)
	{
		$dirHandler = opendir($folderName);
		while ($file = readdir($dirHandler))
		{
			$file = $folderName."\\".$file;
			if (is_dir($file))
				if (basename($file) != '.' && basename($file) != '..')
					read_folder($file, $resultFile);
			
			if (is_file($file))
			{
				$filename = basename($file);
				$path = str_replace("\\", "/", dirname($file));
				$libname = basename($path);
				$resultString = "('$filename', '$path', '$libname'),\n";
				file_put_contents($resultFile, $resultString, FILE_APPEND);
			}
		}
		closedir($dirHandler);
	}
	
	$resultFile = "js.sql";
	$baseDir = "js";
	$beginingSQL = "use greenhops;\n\nINSERT INTO `sys.php_modules` (`FileName`, `PathName`, `LibName`) VALUES\n";
	file_put_contents($resultFile, $beginingSQL);
	read_folder($baseDir, $resultFile);
	// Retirer la derniere virgule
	$fh = fopen($resultFile, 'r+') or die("can't open file");
	$stat = fstat($fh);
	ftruncate($fh, $stat['size']-2);
	fclose($fh);
?>