<?php 


	//////////////////////////////////////////////
	// Includes BDD
	//////////////////////////////////////////////
	
	include_once '../../includes/functions.php';
	
	//////////////////////
	// Démarrage session
	//////////////////////
	
	sec_session_start();
	
	//////////////////////
	// Infos de temps
	//////////////////////
	
	include_once '../../includes/system/time_stuff.php';
	
	if (isset($_POST['emaill'], $_POST['p'])) 
	{    
		$email = $_POST['emaill'];    
		$password = $_POST['p']; 
    	if (login($email, $password) == true) //SI le log se fait
		{
			log_player_action($_SESSION['user_id'],gmdate("Y-m-d H:i:s", $now),0,0);
			//update_player_last_connexion($_SESSION['user_id'], gmdate("Y-m-d H:i:s", $now));
			$urlto='Location: ../../index.php';
		} 
		else 
		{   
			$urlto='Location: ../../index.php?errid=7';    
		} 
	} 
	header($urlto); // Redirection vers la partie connectée ou vers message d'erreur
?>