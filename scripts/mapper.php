<?php
/////////////////////////////////////////////////////////////
// Mapper SQL : Permet a l'utilisateur du moteur d'utiliser les fonctions sql sans avoir a passer par les fonctions mysqli
/////////////////////////////////////////////////////////////

	define ('SQL_AND', 0);
	define ('SQL_OR', 1);
	define ('EQUAL', 2);
	define ('DIFFERENT', 3);
	define ('GREATER', 4);
	define ('LOWER', 5);
	define ('GREATER_OR_EQUAL', 6);
	define ('LOWER_OR_EQUAL', 7);
	define ('NULL_EQUAL', 8);
	define ('LIKE', 9);

	// Renvoie la chaine de caractere correspondant a l'entier passe en parametre
	// Renvoie une chaine vide si l'entier ne fait pas partie des define ci-dessus
	function get_str_from_constant($constant_value)
	{
		switch($constant_value)
		{
			case 0:
				return 'AND';
			case 1:
				return 'OR';
			case 2:
				return '=';
			case 3:
				return '!=';
			case 4:
				return '>';
			case 5:
				return '<';
			case 6:
				return '>=';
			case 7:
				return '<=';
			case 8:
				return '<=>';
			case 9:
				return 'LIKE';
			default:
				return '';
		}
	}
	
	// Verifie que le tableau en entree ne comporte que des operateurs SQL supportes
	// Renvoie true si c'est le cas, false sinon
	function check_conditions($conditions)
	{
		for ($i = 0; $i < count($conditions); $i++)
		{
			if (!isset($conditions[$i])
				|| !is_array($conditions[$i])
				|| count($conditions[$i]) != 3
				|| !is_int($conditions[$i][1])
				|| $conditions[$i][1] < 2
				|| $conditions[$i][1] > 9)
				return false;
		}
		return true;
	}
	
	// Verifie que le tableau en entree ne comporte aue des separateurs SQL supportes
	// Renvoie True si c'est le cas, False sinon
	function checkSeparators($separators)
	{
		for ($i = 0; $i < count($separators); $i++)
			if ($separators[$i] != 0 && $separators[$i] != 1)
				return false;
		return true;
	}
	
	// Fonction pour effectuer un SELECT.
	// Renvoie les donnees selectionnes si tout se passe bien, sinon un code d'erreur.
	// Parametres d'entree :
	//	$mysqli : L'objet mysqli qui va effectuer la requete
	//	$dbEnv : Prefixe de la table visee (str)
	//	$table : Table visee (str)
	//	$fields : Champs de la table vises (tableau de str)
	//	$conditions : Tableau contenant [Champs, operateur, valeur] pour chaque condition. operateur DOIT etre dans la liste des valeurs predefinies.
	//	$conditionSeparators : Tableau de 'AND' et/ou 'OR' permettant de separer les conditions. Si laisse par defaut, se comportera comme un tableau de 'AND'.
	//			La taille du tableau doit toujours etre egale a count($conditions) - 1
	//
	// Code d'erreurs :
	//	-1 : L'objet passe en parametre n'est pas un objet de type mysqli
	//	-2 : Un des parametres $dbEnv, $table, $fields ou $conditions ne repond pas au criteres de la fonction
	//	-3 : Le parametre $conditionSeparators ne repond pas au criteres de la fonction
	//	-5 : Erreur lors de la preparation ou l'execution de la requete sql
	function client_query_db_select($mysqli, $dbEnv, $table, $fields, $conditions = array(), $conditionSeparators = null)
	{
		// Verification des parametres
		if (!($mysqli instanceof mysqli))
			return -1;
			
		if (!is_string($dbEnv)
			|| !is_string($table)
			|| (!is_array($fields) && $fields != '*')
			|| !is_array($conditions)
			|| !check_conditions($conditions))
			return -2;
			
		if (isset($conditionSeparators)
				&& (!is_array($conditionSeparators)
					|| count($conditions) != count($conditionSeparators) + 1
					|| !checkSeparators($conditionSeparators)))
			return -3;
			
		//Construction de la requete
		$TableName = ($dbEnv != '')
			?'`'.$dbEnv.'.'.$table.'`'
			:'`'.$table.'`';

		$Condition='';
		for ($i = 0; $i < count($conditions); $i++)
		{
			if ($Condition != '')
			{
				if (isset($conditionSeparators))
					$Condition .= ' '.get_str_from_constant($conditionSeparators[$i - 1]).' ';
				else
					$Condition .= ' AND ';
			}
			$Condition .= $conditions[$i][0].' '.get_str_from_constant($conditions[$i][1]).' \''.$conditions[$i][2].'\'';
		}
		
		$SelectFields='';
		if (!is_array($fields))
		{
			$SelectFields = '*';
		}
		else
		{
			for ($i = 0; $i < count($fields); $i++)
			{
				if ($SelectFields != '')
					$SelectFields .= ', ';
				$SelectFields .= '`'.$fields[$i].'`';
			}
		}
		
		$rqst="SELECT $SelectFields FROM $TableName";
		if ($Condition != '')
			$rqst .= " WHERE $Condition";

		var_dump($rqst);
		//Execution de la requete
		if (!($stmt = $mysqli->prepare($rqst)))
		{
			var_dump($mysqli->error);
			return -5;
		}

		// creer un tableau de resultat dynamique que l'on utilise pour binder les resultats
		for ($i = 0; $i < count($fields); $i++)
		{
			$var = $i;
			$$var = null;
			$results[$var] = &$$var;
		}
		call_user_func_array(array($stmt,'bind_result'),$results);
		
		$stmt->execute();
		$stmt->store_result();
		$i = 0;
		while ($stmt->fetch())
		{
			for ($j = 0; $j < count($results); $j++)
				$res[$i][]=$results[$j];
			$i++;
		}
		$stmt->close();
		return $res;
	}
	
	// Fonction pour effectuer un INSERT.
	// Renvoie 0 si tout se passe bien, un code d'erreur sinon
	// Parametres d'entree :
	//	$mysqli : L'objet mysqli qui va effectuer la requete
	//	$dbEnv : Prefixe de la table visee (str)
	//	$table : Table visee (str)
	//	$fields : Champs de la table visee devant recevoir une valeur. Tableau de str. Doit avoir une taille egale a count($values).
	//	$values : Valeurs devant etre incluse dans le champs de la table qui correspond. Tableau de str. Doit avoir une taille egale a count($fields).
	//
	// Code d'erreurs :
	//	-1 : L'objet passe en parametre n'est pas un objet de type mysqli
	//	-2 : Un des parametres $dbEnv, $table, $fields ou $conditions ne repond pas au criteres de la fonction
	//	-5 : Erreur lors de la preparation ou l'execution de la requete sql
	function client_query_db_insert($mysqli, $dbEnv, $table, $fields, $values)
	{
		// Verification des parametres
		if (!($mysqli instanceof mysqli))
			return -1;
			
		if (!is_string($dbEnv)
			|| !is_string($table)
			|| !is_array($fields)
			|| !is_array($values)
			|| count($fields) != count($values))
			return -2;
			
		// Creation de la requete
		$TableName = ($dbEnv != '')
			?'`'.$dbEnv.'.'.$table.'`'
			:'`'.$table.'`';
		
		$Pattern='';
		$InsertVals='';
		for ($i = 0; $i < count($fields); $i++)
		{
			if ($Pattern != '')
				$Pattern .= ', ';
			$Pattern .= '`'.$fields[$i].'`';

			if ($InsertVals != '')
				$InsertVals .= ', ';
			$InsertVals .= '\''.$values[$i].'\'';
		}
		
		$rqst="INSERT INTO $TableName ($Pattern)  VALUES ($InsertVals)";
		var_dump($rqst);
		// Execution de la requete
		if ($stmt = $mysqli->prepare($rqst))
		{
			$stmt->execute();       
			$stmt->store_result();
			$stmt->close();
			return 0;
		}
		else
			return -5;
	}
	
	// Fonction pour effectuer un UPDATE.
	// Renvoie 0 si tout se passe bien, sinon un code d'erreur.
	// Parametres d'entree :
	//	$mysqli : L'objet mysqli qui va effectuer la requete
	//	$dbEnv : Prefixe de la table visee (str)
	//	$table : Table visee (str)
	//	$fields : Champs de la table visee devant modifier sa valeur. Tableau de str. Doit avoir une taille egale a count($values).
	//	$values : Valeurs devant etre incluse dans le champs de la table qui correspond. Tableau de str. Doit avoir une taille egale a count($fields).
	//	$conditions : Tableau contenant [Champs, operateur, valeur] pour chaque condition. operateur DOIT etre dans la liste des valeurs predefinies.
	//	$conditionSeparators : Tableau de 'AND' et/ou 'OR' permettant de separer les conditions. Si laisse par defaut, se comportera comme un tableau de 'AND'.
	//			La taille du tableau doit toujours etre egale a count($conditions) - 1
	//
	// Code d'erreurs :
	//	-1 : L'objet passe en parametre n'est pas un objet de type mysqli
	//	-2 : Un des parametres $dbEnv, $table, $fields ou $conditions ne repond pas au criteres de la fonction
	//	-3 : Le parametre $conditionSeparators ne repond pas au criteres de la fonction
	//	-5 : Erreur lors de la preparation ou l'execution de la requete sql
	function client_query_db_update($mysqli, $dbEnv, $table, $fields, $values, $conditions = array(), $conditionSeparators = null)
	{
		// Verification des parametres
		if (!($mysqli instanceof mysqli))
			return -1;
			
		if (!is_string($dbEnv)
			|| !is_string($table)
			|| !is_array($fields)
			|| !is_array($values)
			|| count($values) != count($fields)
			|| !is_array($conditions))
			return -2;
			
		if (isset($conditionSeparators)
				&& (!is_array($conditionSeparators)
					|| count($conditions) != count($conditionSeparators) + 1
					|| !checkSeparators($conditionSeparators)))
			return -3;
			
		//Construction de la requete
		$TableName = ($dbEnv != '')
			?'`'.$dbEnv.'.'.$table.'`'
			:'`'.$table.'`';
		
		$Condition='';
		for ($i = 0; $i < count($conditions); $i++)
		{
			if ($Condition != '')
			{
				if (isset($conditionSeparators))
					$Condition .= ' '.get_str_from_constant($conditionSeparators[$i - 1]).' ';
				else
					$Condition .= ' AND ';
			}
			$Condition .= $conditions[$i][0].' '.get_str_from_constant($conditions[$i][1]).' \''.$conditions[$i][2].'\'';
		}
		
		$UpdateFields='';
		for ($i = 0; $i < count($fields); $i++)
		{
			if ($UpdateFields != '')
				$UpdateFields .= ', ';
			$UpdateFields.='`'.$fields[$i].'` = \''.$values[$i].'\'';
		}
		
		$rqst="UPDATE $TableName SET $UpdateFields";
		if ($Condition!='')
			$rqst .= " WHERE $Condition";
		var_dump($rqst);
		
		if ($stmt = $mysqli->prepare($rqst))
		{
			$stmt->execute();       
			$stmt->store_result();
			$stmt->close();
			return 0;
		}
		else
		{
			return -5;
		}
	}
	
	// Fonction pour effectuer une requete SQL quelconque. Necessite des connaissances en SQL.
	// Renvoie 0 ou des donnes si tout se passe bien, sinon un code d'erreur.
	// Parametres d'entree :
	//	$mysqli : L'objet mysqli qui va effectuer la requete
	//	$request : Chaine de caracteres contenant la requete SQL a exectuer
	//	&$error : reference a une variable. Si donnee en parametrem contiendra le message d'erreur en cas d'erreur.
	//
	// Code d'erreurs :
	//	-1 : L'objet passe en parametre n'est pas un objet de type mysqli
	//	-5 : Erreur lors de la preparation ou l'execution de la requete sql
	function client_query_db ($mysqli, $request, &$error = '')
	{
		if (!($mysqli instanceof mysqli))
		{
			$error = "First parameter isn't a MySLQI instance.";
			return -1;
		}
		
		if ($stmt = $mysqli->prepare($request))
		{
			$stmt->execute();
			// creer un tableau de resultat dynamique que l'on utilise pour binder les resultats
			if ($meta = $stmt->result_metadata())
			{
				$i = 0;
				while ($field = $meta->fetch_field())
					$parameters[] = &$row[$i++];

				call_user_func_array(array($stmt, 'bind_result'), $parameters);

				while ($stmt->fetch())
				{
					foreach($row as $key => $val)
						$x[$key] = $val;
					$results[] = $x;
				}
			}
			
			$stmt->close();
			return (isset($results))?$results:0;
		}
		else
		{
			$error = $mysqli->error;
			return -5;
		}
	}	
?>