<?php 

	include_once '../../includes/functions.php'; 

	sec_session_start();
	
	include_once '../../includes/system/time_stuff.php';
	
	function register($email, $password, $username, $shop, $avatar)
	{
		$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
		
		if (client_query_db($mysqli, 
		"INSERT INTO `pd.player_profile` 
			(`Email`, `ID_Lang`, `Player_Type`, `Active`, `Username`, `Shop`) 
			VALUES 
			('$email', 1, 'Player', 1, '$username', $shop)") != 0)
		{	
			return false;
		}	
		
		$id = get_player_id_from_mail($email);
		$random_salt = hash('sha512', uniqid($username, true)); 
		$ipassword = hash('sha512', $password . $random_salt);
		$token = hash('sha512', uniqid($username, true));
		$time = gmdate("Y-m-d H:i:s", 0);

		if (client_query_db($mysqli,
		"INSERT INTO `pd.player_data`
			(`ID_Player_Profile`, `KPass`, `Last_Connexion`, `Inscription_Date`, `Connexion_Number`, `salt`, `token`)
			VALUES 
			($id,'$ipassword','$time','$time',1,'$random_salt','$token')") != 0)
		{
			return false;
		}
		if (client_query_db($mysqli, 
		"INSERT INTO `gp.character_profile` 
			(`ID_Player`, `Nick`, `Avatar` ) 
			VALUES 
			($id, '$username', '$avatar')") != 0)
		{
			return false;
		}
		
		$tmp[] = $id;
		for ($i = 1; $i <= 16; $i++)
		{
			if ($i == 1)
			{
				$tmp[] = 'inactive-0-1-0';
				$tmp[] = '16-1-20-3-0-0-0-11-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0';
			}
			else
			{
				$tmp[] = 'inactive-0-0-0';
				$tmp[] = '0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0';
			}
		}
		
		$tmp[] = 'inactive-0-'.rand(5,10).'-'.rand(5,10).'-'.rand(5,10).'-'.rand(5,10).'-'.rand(5,10);
		
		for ($i = 1; $i <= 9; $i++)
			$tmp[] = 'inactive-0-0';
		
		$rooms = "`ID_Owner`,`Room_State1`,`Room_Equip1`,`Room_State2`,`Room_Equip2`,`Room_State3`,`Room_Equip3`,`Room_State4`,`Room_Equip4`,`Room_State5`,`Room_Equip5`,
			`Room_State6`,`Room_Equip6`,`Room_State7`,`Room_Equip7`,`Room_State8`,`Room_Equip8`,`Room_State9`,`Room_Equip9`,`Room_State10`,`Room_Equip10`,
			`Room_State11`,`Room_Equip11`,`Room_State12`,`Room_Equip12`,`Room_State13`,`Room_Equip13`,`Room_State14`,`Room_Equip14`,`Room_State15`,`Room_Equip15`,`Room_State16`,`Room_Equip16`,";
		$breweries = "`Brewery_State`,`Brewery_Slot_State1`,`Brewery_Slot_State2`,`Brewery_Slot_State3`,`Brewery_Slot_State4`,`Brewery_Slot_State5`,";
		$friends = "`Friend_Slot_State1`,`Friend_Slot_State2`,`Friend_Slot_State3`,`Friend_Slot_State4`";
		$values = '';
		for($i = 0; $i < count($tmp); $i++)
		{
			if ($values != '')
				$values .= ',';
			$values .= "'{$tmp[$i]}'";
		}
		$request = "INSERT INTO `gh.env_specs` ($rooms$breweries$friends) VALUES ($values)";
		
		if (client_query_db($mysqli, $request) != 0)
			return false;
		
		$fields = "`ID_Character`, 
					`ID_Owner`, 
					`Niveau`, 
					`points`, 
					`science`, 
					`social`, 
					`discretion`, 
					`debit`, `credit`, 
					`MarketLvl`, 
					`StockLvl`, 
					`Houblonstock`, 
					`Inventory`";
		$values = "$id,
					$id,
					1, 
					0, 
					50, 
					50, 
					50, 
					-5000, 
					15000, 
					1, 
					1, 
					'0-0-0-0-0', 
					'0-1-0-1-0-0-0-1-0-0-0-1-0-0-0-0-1-0-0-0-1-0-0-0-0-0-1-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0'";
		
		if (client_query_db($mysqli, "INSERT INTO `gh.character_specs` ($fields) VALUES ($values)") != 0)
			return false;
		
		//TODO add succes & action log
		
		return $id;
	}
	
	$error_msg="";
	// Filtrage des input
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
	$password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'emailr', FILTER_SANITIZE_EMAIL);   
	$shop =  filter_input(INPUT_POST, 'shop', FILTER_SANITIZE_NUMBER_INT); 
	$avatar =  filter_input(INPUT_POST, 'avatar', FILTER_SANITIZE_STRING);

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
	{               
		$error_msg = '1'; // Souci email    
	}     
	if (strlen($password) > 128 || strlen($password) < 4)
	{               
		$error_msg = '2'; // Souci mdp;    
	} 
	if (!isset($_POST['acceptance']))
	{
		$error_msg = '3'; //Conditions nok
	}
	if (($shop<1) && ($shop>109))
	{
		$error_msg = '4'; //shop nok
	}
	
	if (is_email_used($email))
		$error_msg = '6';
	
	if ($error_msg=="")
	{        
   		if (register($email, $password, $username, $shop, $avatar) != false)
			header('Location: ../../index.php?val=1');
		else
			header('Location: ../../index.php?errid=5');
	}
	else
	{
		header('Location: ../../index.php?errid='.$error_msg);
	}

?>
      
