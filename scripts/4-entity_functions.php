<?php


/////////////////////////////////////
// 4.1 Fonction Entités : Sélection d'une caractéristique numérique d'un item - renvoies un nombre 
/////////////////////////////////////

function get_item_numeric_property($ItemID,$Property)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `N_Val` 
		FROM `gp.game_elements_specs` 
		WHERE `ID_Property`= ( 
			SELECT gep.`ID` 
				FROM `gp.game_elements_properties` gep 
				JOIN `i18n.translation_text` tt 
				ON tt.`ID_I18n_Regroup` = gep.`I18n_Label_ID` 
				WHERE tt.`T_Val` = '$Property'
					AND tt.`ID_Lang` = 1) 
		AND `ID_Parent_Element` = $ItemID");
	return $tmp[0][0];
}

/////////////////////////////////////////
// 4.2 Fonction Entités : Mise à jour de la quantité d'un item - renvoies true ou false
/////////////////////////////////////////

function update_item_qty($uid, $itemID, $qty)
{
	//Récupération tableau d'inventaire
	$inventory=get_player_inventory($uid); // Tableau X éléments représentant les quantités des élements avec index=id
	$index=$itemID;//base indexage tableau=0
	if (($inventory[$itemID]+$qty)>=0) //Nanan pas d'ardoise,le stock reste positif
	{
		$inventory[$index]+=$qty;
	}
	$updatestr="";
	for ($n=0;$n<50;$n++)
	{
		$updatestr.=$inventory[$n]."-";
	}
	$updatestr.=$inventory[50];
	
	if (client_query_db($_SESSION['mysqli'], 
	"UPDATE `gh.character_specs` 
		SET `Inventory` = '$updatestr' 
		WHERE `ID_Character`= $uid") == 0)
		return true;
	return false;
}
/////////////////////////////////////
// 4.3 Fonction Entités : Récupération de la catégorie d'un item - renvoies un nombre
/////////////////////////////////////

function get_item_category ($ItemID)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `ID_Cat` 
		FROM `gp.game_elements` 
		WHERE `ID`= $ItemID");
	return $tmp[0][0];
}

/////////////////////////////////////
// 4.4 Fonction Entités : Récupération du nom d'un item - renvoies une chaine
/////////////////////////////////////

function get_item_name ($ItemID)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `I18n_Label_ID` 
		FROM `gp.game_elements` 
		WHERE `ID`= $ItemID");
	return get_translated_label($tmp[0][0], $_SESSION['lang']); 
}

/////////////////////////////////////
// 4.5 Fonction Entités : Récupération de l'image d'un item - renvoies une chaine
/////////////////////////////////////

function get_item_img ($ItemID)
{
	$tmp = client_query_db($_SESSION['mysqli'],
	"SELECT p.`PathName`, i.`FileName`, it.`PicExtension`
		FROM `sys.imgs` i 
		JOIN `sys.paths` p 
		ON i.`Path_ID` = p.`ID`
		JOIN `sys.imgs_types` it 
		ON i.`ImgType_ID` = it.`ID`
		WHERE i.`ID` = (
			SELECT `ID_Img` 
				FROM `gp.game_elements` 
				WHERE `ID`= $ItemID)");
	return $tmp[0][0].$tmp[0][1].$tmp[0][2]; 
}

/////////////////////////////////////
// 4.6 Fonction Entités : Récupération du nom d'un item - renvoies un tableau [][Label, N_Val]
/////////////////////////////////////

function get_item_properties ($ItemID)
{
	$tmp = client_query_db($_SESSION['mysqli'],
	"SELECT gep.`I18n_Label_ID`, ges.`N_Val`
		FROM `gp.game_elements_specs` ges 
		JOIN `gp.game_elements_properties` gep 
		ON ges.`ID_Property` = gep.`ID`
		WHERE `ID_Parent_Element`= $ItemID");
	for ($i = 0; $i < count($tmp); $i++)
	{
		$ret[get_translated_label($tmp[$i][0], $_SESSION['lang'])] = round($tmp[$i][1], 2);
	}
	return $ret;
}

/////////////////////////////////////
// 4.7 Fonction Entités : Récupération des identifiants des objects appartenant a un catalogue - renvoies un tableau
/////////////////////////////////////

function get_items_from_cat($Category)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `ID` 
		FROM `gp.game_elements` 
		WHERE `ID_Cat`= $Category");
	return $tmp;
}

?>