<?php


/////////////////////////////////////
// 1.1 Fonction Système - Logges le joueur
/////////////////////////////////////

function login($email, $password) 
{   
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	
	$tmp = client_query_db($mysqli, 
	"SELECT `ID`,`Username` 
		FROM `pd.player_profile` 
		WHERE `Email` = '$email'");
	$user_id = $tmp[0][0];
	$username = $tmp[0][1];

	$tmp = client_query_db($mysqli, 
	"SELECT `KPass`, `salt` 
		FROM `pd.player_data` 
		WHERE `ID_Player_Profile` = $user_id");
	$spassword = $tmp[0][0];
	$salt = $tmp[0][1];
	$db_password = hash('sha512', $password . $salt);
	
	if ($db_password == $spassword) 
	{                                    
		$user_browser = $_SERVER['HTTP_USER_AGENT'];                                   
		$user_id = preg_replace("/[^0-9]+/", "", $user_id);                    
		$_SESSION['user_id'] = $user_id;
		$username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);                    
		$_SESSION['username'] = $username;
		$_SESSION['mail']  = $email;               
		$_SESSION['login_string'] = hash('sha512', $password . $user_browser);
		$_SESSION['lang'] = get_lang_from_player($user_id);
		
		// IP 
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_CLIENT_IP']))
			$ip  = $_SERVER['HTTP_CLIENT_IP'];
		else
			$ip = $_SERVER['REMOTE_ADDR'];
		//Machine (OS)
		$host = gethostbyaddr($ip);
		// Navigateur
		$nav = $_SERVER['HTTP_USER_AGENT'];

		$_SESSION['sysval'] = get_sysval_id($user_id, $ip, $nav, $host);
		return true;                
	}
	return false;
}

/////////////////////////////////////
// 1.2 Fonction Système - Vérifies si le joueur est loggé
/////////////////////////////////////

function login_check() 
{    
	if (isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) 
	{ 
		$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
        $uid = $_SESSION['user_id'];        
		$login_string = $_SESSION['login_string'];        
		$username = $_SESSION['username'];       
		$user_browser = $_SERVER['HTTP_USER_AGENT']; 
		$tmp = client_query_db($mysqli, 
		"SELECT `Username` 
			FROM `pd.player_profile` 
			WHERE `ID` = $uid");
		if (is_array($tmp)) 
		{ 
			return true ;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
} 

/////////////////////////////////////
// 1.3 Fonction Système - Démarres une session
/////////////////////////////////////

function sec_session_start() 
{    
	$session_name = 'greenhops_session_id';     
	$secure = false;       
	$httponly = true;    
	$cookieParams = session_get_cookie_params();    
	session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"],$secure,$httponly);
	session_name($session_name);
	session_start();                
	session_regenerate_id(); 
	$_SESSION['mysqli'] = new mysqli(HOST, USER, PASSWORD, DATABASE);
} 

/////////////////////////////////////
// 1.4 Fonction Système : Epuration d'URL - renvoies une chaîne URL épurée
/////////////////////////////////////

function esc_url($url) 
{ 
    if ('' == $url) 
	{        
		return $url;    
	} 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url); 
    $strip = array('%0d', '%0a', '%0D', '%0A');    $url = (string) $url; 
    $count = 1;    
	while ($count) 
	{       
		$url = str_replace($strip, '', $url, $count);    
	} 
    $url = str_replace(';//', '://', $url); 
    $url = htmlentities($url); 
    $url = str_replace('&amp;', '&#038;', $url);    
	$url = str_replace("'", '&#039;', $url); 
    if ($url[0] !== '/') 
	{             
		return '';    
	} 
	else 
	{        
		return $url;    
	} 
}

/////////////////////////////////////
// 1.5 Fonction Système - renvoies l'ID entier d'un joueur selon son Email
/////////////////////////////////////

function get_player_id_from_mail($mail)
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$tmp = client_query_db($mysqli, 
	"SELECT `ID` 
		FROM `pd.player_profile` 
		WHERE `Email` = '$mail'");
	return (is_array($tmp))?$tmp[0][0]:null;
}
/////////////////////////////////////
// 1.6 Fonction Système - renvoies le token de sécurité associé à un joueur
/////////////////////////////////////

function get_player_token($uid)
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$tmp = client_query_db($mysqli, 
	"SELECT `token` 
		FROM `pd.player_data` 
		WHERE `ID_Player_Profile` = $uid ");
	return (is_array($tmp))?$tmp[0][0]:null;
}

/////////////////////////////////////
// 1.7 Fonction Système - renvoies le token de sécurité associé à un joueur
/////////////////////////////////////

function renew_player_token($uid)
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$NewToken=hash('sha512', $uid.'greenhops23');
	if (client_query_db($mysqli, 
	"UPDATE `pd.player_data` 
		SET `token` = $NewToken 
		WHERE `ID_Player_Profile` = $uid") == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////
// 1.8 Fonction Système - Mets à jour le mot de passe
/////////////////////////////////////

function update_kpass( $kpass , $uid )
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$salt = client_query_db($mysqli, 
	"SELECT `salt` 
		FROM `pd.player_data` 
		WHERE `ID_Player_Profile` = $uid ");
	$ipassword = hash('sha512', $kpass.$salt);
	if (client_query_db($mysqli, 
	"UPDATE `pd.player_data` 
		SET `KPass` = $ipassword 
		WHERE `ID_Player_Profile` = $uid") == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////
// 1.9 Fonction Système : Sélection du temps d'action - renvoies un nombre entier en secondes
/////////////////////////////////////

function get_action_time($action)
{
	$ActionTime = client_query_db($_SESSION['mysqli'], 
	"SELECT `Cycle_s` 
		FROM `ev.actions` 
		WHERE `ID_Action`= $action");
	return (is_array($ActionTime))?$ActionTime[0][0]:0;
}

/////////////////////////////////////
// 1.10 Fonction Système : Charges en mémoire les variables  Générales du jeu
/////////////////////////////////////

function load_game_env($AppName)
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$Settings = client_query_db($mysqli, 
	"SELECT `I18n_Label_ID`, `T_Val`, `N_Val`, `A_Val` 
		FROM `sys.global_game_settings` 
		WHERE `AppName` = $AppName");
	$_SESSION['params'][$Settings[0][0]][0] = $Settings[0][1];
	$_SESSION['params'][$Settings[0][0]][1] = $Settings[0][2];
	$_SESSION['params'][$Settings[0][0]][2] = explode('-',$Settings[0][3]);
}

/////////////////////////////////////
// 1.11 Fonction Interface : Charges le contrôleur d'environnement spécifique
/////////////////////////////////////

function load_env_controller($env_id)
{
	$ControllerName = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, pm.`Filename` 
		FROM `sys.php_modules` pm 
		JOIN `sys.paths` p 
		ON pm.`Path_ID` = p.`ID` 
		WHERE pm.`ID` = '$env_id' ");
    if (file_exists($ControllerName[0][0].$ControllerName[0][1].'.php'))
		return $ControllerName[0][0].$ControllerName[0][1].'.php';
	return '';
}

/////////////////////////////////////
// 1.12 Fonction Interface : Charges les librairies Javascript d'un environnement
/////////////////////////////////////

function load_env_js()
{
	$JsFilesPathList = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, jl.`FileName` 
		FROM `sys.js_libs` jl 
		JOIN `sys.paths` p 
		ON jl.`Path_ID` = p.`ID`");
	for($i = 0; $i < count($JsFilesPathList); $i++)
	{
		if (file_exists($JsFilesPathList[$i][0].$JsFilesPathList[$i][1].'.js'))
			$ret[] = $JsFilesPathList[$i][0].$JsFilesPathList[$i][1].'.js';
	}
	return isset($ret)?$ret:null;
}

/////////////////////////////////////
// 1.13 Fonction Interface : liste les enfants d'un environnement - param ParentID spécifies le parent,0 pour les env root
/////////////////////////////////////

function load_generic_js()
{
	$JsFilesGenericPathList = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, jl.`FileName` 
		FROM `sys.js_libs` jl 
		JOIN `sys.paths` p 
		ON jl.`Path_ID` = p.`ID` 
		WHERE jl.`More` = 'generic' 
		ORDER BY jl.`ID`");
	for($i = 0; $i < count($JsFilesGenericPathList); $i++)
	{
		if (file_exists($JsFilesGenericPathList[$i][0].$JsFilesGenericPathList[$i][1].'.js'))
			$ret[] = $JsFilesGenericPathList[$i][0].$JsFilesGenericPathList[$i][1].'.js';
	}
	return isset($ret)?$ret:null;
}

/////////////////////////////////////
// 1.14 Fonction Interface : Charges les Styles CSS génériques
/////////////////////////////////////

function load_generic_style()
{
	$CssFilesGenericPathList = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, cs.`FileName` 
		FROM `sys.css_styles` cs 
		JOIN `sys.paths` p 
		ON cs.`Path_ID` = p.`ID` 
		WHERE cs.`More` = 'generic'");
	for($i = 0; $i < count($CssFilesGenericPathList); $i++)
	{
		if (file_exists($CssFilesGenericPathList[$i][0].$CssFilesGenericPathList[$i][1].'.css'))
			$ret[] = $CssFilesGenericPathList[$i][0].$CssFilesGenericPathList[$i][1].'.css';
	}
	return isset($ret)?$ret:null;
}

/////////////////////////////////////
// 1.15 Fonction Interface : Charges les Styles CSS des librairies Javascript génériques
/////////////////////////////////////

function load_generic_js_style()
{
	$CssFilesPathList = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, cs.`FileName` 
		FROM `sys.css_styles` cs 
		JOIN `sys.paths` p 
		ON cs.`Path_ID` = p.`ID` 
		WHERE cs.`More` = 'js'");
	for($i = 0; $i < count($CssFilesPathList); $i++)
	{
		if (file_exists($CssFilesPathList[$i][0].$CssFilesPathList[$i][1].'.css'))
			$ret[] = $CssFilesPathList[$i][0].$CssFilesPathList[$i][1].'.css';
	}
	return isset($ret)?$ret:null;
}

/////////////////////////////////////
// 1.16 Fonction Interface : Charges les Styles CSS d'un Environnement
/////////////////////////////////////

function load_module_style($EnvID)
{
	$CssFilePath = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, cs.`FileName` 
		FROM `sys.css_styles` cs 
		JOIN `sys.paths` p 
		ON cs.`Path_ID` = p.`ID` 
		WHERE cs.`ID_Parent_Env` = $EnvID");
	if (file_exists($CssFilePath[$i][0].$CssFilePath[$i][1].'.css'))
		return $CssFilePath[$i][0].$CssFilePath[$i][1].'.css';
	return '';
}

/////////////////////////////////////
// 1.17 Fonction Système : retournes le chemin de l'image associée à un état selon son nom
/////////////////////////////////////

function get_state_pic_from_str($state)
{
	$tmp = client_query_db($_SESSION['mysqli'],
	 "SELECT `PathName`, `Filename`, `PicExtension`
		FROM `sys.imgs` i 
		JOIN `sys.imgs_types` it 
		ON it.`ID` = i.`ImgType_ID`
		JOIN `sys.paths` p 
		ON p.`ID` = i.`Path_ID`
		WHERE `Filename` = '$state'");
	return $tmp[0][0].$tmp[0][1].'.'.$tmp[0][2];
}

/////////////////////////////////////
// 1.18 Fonction Système : Renvoie true si un utilisateur possede l'adresse mail, false sinon
/////////////////////////////////////

function is_email_used($email)
{
	$mysqli= new mysqli(HOST, USER, PASSWORD, DATABASE);
	$tmp = client_query_db($mysqli, 
	"SELECT COUNT(*) 
		FROM `pd.player_profile` 
		WHERE `Email` = '$email'");
	if ($tmp[0][0] != 0)
		return true;
	return false;
}

/////////////////////////////////////
// 1.19 Fonction Système : Renvoie true si un utilisateur possede l'adresse mail, false sinon
/////////////////////////////////////

function list_env_from($ParentID)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `Link`, `I18n_Label_ID` 
		FROM `sys.environments` 
		WHERE `ID_Parent` = $ParentID");
	return $tmp;
}

/////////////////////////////////////
// 1.18 Fonction Système : Retourne la chaine correspondant a l'etat actuel de la room dans la langue de l'user
/////////////////////////////////////

// Working function, waiting the completion of the database
// function get_translated_state_name_from_str($rstate)
// {
	// $id = "SELECT `ID_I18n_Regroup` FROM `i18n.translation_text` WHERE `T_Val` = '$rstate'";
	// $request = "SELECT `T_Val` FROM `i18n.translation_text` WHERE `ID_I18n_Regroup` = ($id) AND `ID_Lang` = {$_SESSION['lang']}";
	// $tmp = client_query_db($_SESSION['mysqli'], $request);
	// return $tmp[0][0];
// }

// Temporary Solution
function get_translated_state_name_from_str($rstate)
{
	return $rstate;
}

/////////////////////////////////////
// 1.18 Fonction Système : Retourne le fichier js associe a un module
/////////////////////////////////////

function get_associated_js($mid)
{
	$js_id = client_query_db($_SESSION['mysqli'], 
	"SELECT `JS_ID` 
		FROM `sys.environments` 
		WHERE `ID_MENU` = $mid");
	$js_file = client_query_db($_SESSION['mysqli'], 
	"SELECT p.`PathName`, jl.`FileName` 
		FROM `sys.js_libs` jl 
		JOIN `sys.paths` p 
		ON jl.`Path_ID` = p.`ID` 
		WHERE jl.`ID` = {$js_id[0][0]}");
	if (file_exists($js_file[0][0].$js_file[0][1].'.js'))
		return $js_file[0][0].$js_file[0][1].'.js';
}

/////////////////////////////////////
// 1.19 Fonction Système : Renvoies les threads enfant de $threadId
/////////////////////////////////////

function get_forum_threads($threadId)
{
	$request = "SELECT `ID`, `Title`, `Description`, `ID_Owner`, `ID_Icon`, `Thread_nb`, `Publication_Moment`
		FROM `plugin.forum_threads`
		WHERE `ID_Parent` <=> $threadId";
	$tmp = client_query_db($_SESSION['mysqli'], $request);
	return $tmp;
}

function get_newly_created_thread($lang, $title, $time, $uid)
{
	$tmp = client_query_db($_SESSION['mysqli'],
		"SELECT `ID`
		FROM `plugin.forum_threads`
		WHERE `Title` = '$title'
			AND `Publication_Moment` = '$time'
			AND `ID_Owner` = $uid");
	return $tmp[0][0];
}

function create_forum_thread($title, $description, $message, $parent, $uid, $icon, $time)
{
	if (client_query_db($_SESSION['mysqli'],
		"INSERT INTO `plugin.forum_threads`
			(`Title`, `Description`, `ID_Parent`, `ID_Owner`, `ID_Icon`, `Answers`, `Publication_Moment`)
			VALUES
			('$title', '$description', $parent, $uid, $icon, 0, '$time')") == 0)
	{
		$id_thread = get_newly_created_thread($lang, $title, $time, $uid);
		if (client_query_db($_SESSION['mysqli'],
			"INSERT INTO `plugin.forum_messages`
				(`ID_Parent`, `ID_Author`, `ID_Icon`, `Title`, `T_Val`, `Publication_Time`, `Article_Number`)
				VALUES
				($id_thread, $uid, NULL, '$title', '$message', '$time', 1)", $error) == 0)
			return true;
		return false;
	}
	else
		return false;
}

/////////////////////////////////////
// 1.20 Fonction Système : Renvoies les articles enfant de $threadId
/////////////////////////////////////

function get_forum_articles($threadId)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `ID`, `ID_Author`, `ID_Icon`, `Title`, `T_Val`, `Publication_Time`, `Article_Number`
		FROM `plugin.forum_messages` 
		WHERE `ID_Parent` = $threadId");
	return $tmp;
}

/////////////////////////////////////
// 1.21 Fonction Système : Enregistre un article
/////////////////////////////////////

function save_article($uid, $thread, $icon, $title, $message, $time)
{
	$tmp = client_query_db($_SESSION['mysqli'],
		"SELECT Count(*)
			FROM `plugin.forum_messages`
			WHERE `ID_Parent` = $thread");
	$num = $tmp[0][0] + 1;
	if (client_query_db($_SESSION['mysqli'],
	 "INSERT INTO `plugin.forum_messages`
		(`ID_Parent`, `ID_Author`, `ID_Icon`, `Title`, `T_Val`, `Publication_Time`, `Article_Number`)
		VALUES
		($thread, $uid, NULL, '$title', '$message', '$time', $num)") == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

/////////////////////////////////////
// 1.21 Fonction Système : Selectionne un article
/////////////////////////////////////

function get_editables_from_article($num, $thread)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `ID_Icon`, `Title`, `T_Val` 
		FROM `plugin.forum_messages` 
		WHERE `Article_Number` = $num
		AND `ID_Parent` = $thread");
	return $tmp[0];
}

/////////////////////////////////////
// 1.21 Fonction Système : Edite un article
/////////////////////////////////////

function edit_article($num, $thread, $title, $message, $icon, $time)
{
	if (client_query_db($_SESSION['mysqli'],
	 "UPDATE `plugin.forum_messages` 
		SET `ID_Icon` = $icon, 
		`Title` = '$title', 
		`T_Val` = '$message'
		WHERE `Article_Number` = $num 
		AND `ID_Parent` = $thread") == 0)
	{
		return true;	
	}
	else
	{
		return false;	
	}
}

?>