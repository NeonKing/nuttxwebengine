<?php


	/////////////////////////////////////
	// Variables globales
	/////////////////////////////////////
	
	$AppName='greenhops'; // Nom application /!\ NECESSAIRE /!\
	
	$SystemModulesPath='modules/system/'; // Contrôleurs Système
	$PluginsPath='modules/plugins/';
	$ModulesPath='modules/'.$AppName.'/'; // Contrôleurs Modules Application
	$SystemViews='templates/system/'; // Vues Système
	$SystemFunctions='includes/'; // Fonctions Green Engine
	$AppViews='templates/'.$AppName.'/'; // Vues Application
	$JsLibs='js/libs/'; // Librairies JavaScript
	$AppJs='js/libs/'.$AppName.'/'; // Javascript Contrôleurs Modules Application
	$GraphicTheme='css/themes/'.$AppName.'/'; // Theme Application
	$ModulesTheme='css/modules'; // Themes Modules Application
	$UploadJail='files/upload_jail/'; // Prison de réception Upload
	$PicturesPath='imgs/'; // Chemin des ressources graphiques
	
	include_once $SystemFunctions.'functions.php'; 	// Include Modèle /!\ NECESSAIRE /!\

	load_game_env($AppName);

	include_once $SystemFunctions.$AppName.'.php';
	include_once $SystemFunctions.'system/time_stuff.php';// Infos Temps
	sec_session_start();// Démarrage session

	if (isset($_SESSION['user_id']))
	{
		$user_id=$_SESSION['user_id'];
		$uid=intval($user_id); // identifiant joueur
	}

	// Paramètres GET
	$Action = intval(filter_input(INPUT_GET, 'id',  FILTER_SANITIZE_NUMBER_INT)); // Identifiant action
	$MenuID=intval(filter_input(INPUT_GET, 'mid',  FILTER_SANITIZE_NUMBER_INT)); // Identifiant Menu
	
	////////////////////////////////////////////
	// Mise en forme et Interfaçage
	///////////////////////////////////////////

	// Routing
	if (login_check() == true)
	{
		include_once($ModulesPath.'global_vars.php'); // global variables
		include_once $SystemViews.'ui.php'; // Gestionnaire Erreur	
		include_once $AppViews.'header.php'; // Header
		include_once $ModulesPath.'auto_update.php'; // Gestionnaire de Mise à Jour Auto des états
		include_once $SystemFunctions.'system/ev_handler.php'; // Gestionnaire Erreurs
		include_once $AppViews.'ingame.php'; // Template en jeu
		include_once $AppViews.'footer.php'; // Footer
	}
	else // Routing par défaut
	{
		if ((isset($MenuID))&&($MenuID!="")) // Exceptions Menuid
		{
			include_once($ModulesPath.'global_vars.php'); // global variables
			include_once $SystemViews.'header.php'; // Header
			include_once $SystemFunctions.'system/ev_handler.php'; // Gestionnaire Erreurs
			include_once $SystemViews.'ui.php'; // Gestionnaire Interface	
			
			switch ($MenuID) 
			{
				 case 11: // Règlement
				 	$articles = get_forum_articles(12);
					for ($i = 0; $i < count($articles); $i++)
					{
						echo '<div style="color:#333">';
						echo '<H1>'.$articles[$i][3].'</H1>';
						echo '<p>'.$articles[$i][4].'</p>';	
						echo '</div>';
					}	
				 	break;
				 case 12: // Pass oublié
				 	include_once $SystemViews."ask_new_pass.php";
				 	break;
				 case 13: // Cadeaux
				 	include_once $AppViews.'gifts.php';
				 	// Lister Cadeaux
				 	break;
				 case 14: // Forum
				 	include_once "modules/plugins/forum.php";
				 	break;
				 case 15: // Inscription
				 	include_once $AppViews."register_form.php";
				 	break;
				 case 17: // Technologies
				 	include_once $SystemViews."technologies.php";
				 	break;
				 default:
				 	break;				 
			}
			include_once $SystemViews.'footer.php'; // footer		
		}
		else
		{
			include_once $ModulesPath.'global_vars.php'; // global variables
			include_once $SystemViews.'header.php'; // Header
			include_once $SystemFunctions.'system/ev_handler.php'; // Gestionnaire Erreurs
			include_once $SystemViews.'ui.php'; // Gestionnaire Erreur
			include_once $AppViews.'home.php';
			include_once $SystemViews.'footer.php';
		}
	}

?>