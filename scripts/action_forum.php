<?php
	include_once '../../includes/functions.php';
	include_once '../../includes/greenhops.php';
	
	sec_session_start();

	include_once '../../includes/system/time_stuff.php';

	include_once '../greenhops/global_vars.php';
	
	if (isset($_GET['id']))
	{
		$uid = $_SESSION['user_id'];
		$action = intval(filter_input(INPUT_GET, 'id',  FILTER_SANITIZE_NUMBER_INT));
		
		switch ($action)
		{
			case 1: // new thread
				$title = filter_input(INPUT_POST, 'title',  FILTER_SANITIZE_STRING);
				$desc = filter_input(INPUT_POST, 'desc',  FILTER_SANITIZE_STRING);
				$msg = filter_input(INPUT_POST, 'msg',  FILTER_SANITIZE_STRING);
				$parent = intval(filter_input(INPUT_POST, 'parent',  FILTER_SANITIZE_NUMBER_INT));
				$icon = 0; // TODO To complete later
				create_forum_thread($title, $desc, $msg, $parent, $uid, $icon, gmdate("Y-m-d H:i:s", $now));
				break;
			case 2: // new article
				$title = filter_input(INPUT_POST, 'title',  FILTER_SANITIZE_STRING);
				$msg = filter_input(INPUT_POST, 'msg',  FILTER_SANITIZE_STRING);
				$icon = 0; // TODO To complete later
				$parent = intval(filter_input(INPUT_POST, 'parent',  FILTER_SANITIZE_NUMBER_INT));
				save_article($uid, $parent, $icon, $title, $msg, gmdate("Y-m-d H:i:s", $now));
				break;
			case 3: // edit article
				break;
		}
	}
	
	header("Location: ../../index.php?mid=8&thread=$parent");
	exit;
?>