<?php


	//////////////////////////////////////////////
	// Includes BDD
	//////////////////////////////////////////////
	
	include_once '../../includes/functions.php'; 
	
	//////////////////////
	/// Démarrage session
	//////////////////////
	
	sec_session_start();
	
	$uid=intval(filter_input(INPUT_POST, 'id',  FILTER_SANITIZE_NUMBER_INT));
	$password=filter_input(INPUT_POST, 'p',  FILTER_SANITIZE_STRING);
	
	update_kpass($password,$uid);
	
	header('Location: ../index.php?val=9'); // Redirection vers le message de validation
?>