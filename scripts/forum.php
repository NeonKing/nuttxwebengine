<script type="text/javascript">
	$(function() {
		$( "#BarTitle").text("Forum");
		$( "#EnvIcon").css('background', "url('imgs/greenhops/icons/forum.png') no-repeat");
		$( "#EnvIcon").css('background-size', 'contain');
		});
</script>
<?php
		mb_encode_numericentity($tmp[0][0], array(0x80, 0xff, 0, 0xff));
	// Forum
	
	// Params GET
	$thread = intval(filter_input(INPUT_GET, 'thread',  FILTER_SANITIZE_NUMBER_INT)); // Thread parent
	$edit = intval(filter_input(INPUT_GET, 'edit',  FILTER_SANITIZE_NUMBER_INT)); // Référence Msg à éditer
	$reply = intval(filter_input(INPUT_GET, 'reply',  FILTER_SANITIZE_NUMBER_INT)); // Référence Msg auquel on répond
	$fid= intval(filter_input(INPUT_GET, 'fid',  FILTER_SANITIZE_NUMBER_INT)); // Identifiant Action forum

	// Routing
	switch ($fid)
	{
		case 1: // Nouveau thread enfant
			// Creer formulaire avec titre, description, contenu message. enregistrer dans forum_threads ET articles
			echo "
			<form action='modules/plugins/action_forum.php?id=1' method='post' id='new_thread_form'>
				<input type='hidden' name='parent' value='$thread'>
				Titre : <input type='text' name='title' required><br>
				Description : <input type='text' name='desc'><br>
				Message :<br><textarea rows='4' cols='50' name='msg' required></textarea><br>
				<input type='submit'>
			</form>";
			break;
		case 2: // Nouveau message dans thread
			echo "
			<form action='modules/plugins/action_forum.php?id=2' method='post' id='reply_form'>
				<input type='hidden' name='parent' value='$thread'>
				Titre : <input type='text' name='title'><br>
				Message :<br><textarea rows='4' cols='50' name='msg' required></textarea><br>
				<input type='submit'>
			</form>";
		case 3: // Réponse
		case 4: // Edit
			// Créer interface avec niceedit message + entête msg précédent/ Thread parent si nécéssaire + contenu niceedit en edit
			
			break;
		default: // Par défaut on liste les threads racine
			$threads = get_forum_threads($thread);
			if ($threads != 0)
			{
				for ($i=0;$i<count($threads);$i++)
				{
					echo '<a href="index.php?mid=8&thread='.$threads[$i][0].'" class="forumthread">';
					echo "<h3>".$threads[$i][1]."</h3><br>
						<p>".$threads[$i][2]."</p>";
					echo '</a>';
				}
			}
			else // afficher articles
			{
				$articles = get_forum_articles($thread);
				for ($i = 0; $i < count($articles); $i++)
				{
					echo "<div class='forumthread'><div class='author'>".get_player_username($articles[$i][1])
						.$articles[$i][5]."</div>
						<div class='content'>";
					if ($articles[$i][3] != NULL)
						echo "<p class='title'>".$articles[$i][3]."</p>";
					echo "<p class='text'>".$articles[$i][4]."</p>
						<p class='num'>{$articles[$i][6]}</p></div></div>";
				}
			}
			if ($thread)
			{
				if ($thread == 1 || $thread == 2)
					echo "<a href='index.php?mid=8&thread=$thread&fid=1'>Nouvelle discussion</a>";
				else
					echo "<a href='index.php?mid=8&thread=$thread&fid=2'>Repondre</a>";
			}
			break;
	}
/*
	if ($edit || $reply)
	{
		if ($edit == 1)
		{
			$id = intval(filter_input(INPUT_GET, 'article',  FILTER_SANITIZE_NUMBER_INT));
			$editables = get_editables_from_article($id, $thread);
			echo "<form action='index.php?mid=8&thread=$thread&article=$id&edit=2' method='post'>
				<input type='hidden' name='type' value='edit'>
				Titre : <input type='text' name='title' value='{$editables[1]}'>
				Votre Message : <input type='text' name='message' value='{$editables[2]}'>
				<input type='submit'>";
		}
		else if ($reply == 1)
		{
			echo "<form action='index.php?mid=8&thread=$thread&reply=2' method='post'>
				<input type='hidden' name='type' value='reply'>
				Titre : <input type='text' name='title'>
				Votre Message : <input type='text' name='message'>
				<input type='submit'>";
		}
		else if ($edit == 2)
		{
			if (isset($_POST))
			{
				$id = intval(filter_input(INPUT_GET, 'article',  FILTER_SANITIZE_NUMBER_INT));
				$title = isset($_POST['title'])?$_POST['title']:'';
				$message = isset($_POST['message'])?$_POST['message']:'';
				$icon = isset($_POST['icon'])?$_POST['icon']:0;
				edit_article($id, $thread, $title, $message, $icon, $now);
			}
			header("Location: index.php?mid=8&thread=$thread");
		}
		else if ($reply == 2)
		{
			if (isset($_POST))
			{
				if (isset($_POST['message']))
				{
					$title = isset($_POST['title'])?$_POST['title']:'';
					$message = $_POST['message'];
					$icon = isset($_POST['icon'])?$_POST['icon']:0;
					save_article($uid, $_SESSION['lang'], $thread, $icon, $title, $message, $now);
				}
			}
			header("Location: index.php?mid=8&thread=$thread");
		}
	}
	else
	{
		if ($thread != 0)
			$threads = get_forum_threads(intval($_GET['thread']));
		else
			$threads = get_forum_threads(NULL);
		
		if ($threads == 0)
		{
			$articles = get_forum_articles($_GET['thread']);
			
			for ($i = 0; $i < count($articles); $i++)
			{	
				$id = $articles[$i][0];
				$author = get_player_username($articles[$i][1]);
				$icon = $articles[$i][2];
				$title = $articles[$i][3];
				$text = $articles[$i][4];
				$publicationTime = gmdate("Y-m-d H:i:s", $articles[$i][5]);
				echo "<p><img src='$icon'>$title</p><p>$text</p><p>$author at $publicationTime</p><br>";
				if ($author == $_SESSION['username'])
					echo "<a href='index.php?mid=8&thread={$_GET['thread']}&article=$id&edit=1'>Edit</a>";
			}
			echo "<a href='index.php?mid=8&thread={$_GET['thread']}&reply=1'>Reply</a>";
		}
		else
		{
			for ($i = 0; $i < count($threads); $i++)
			{
				$id = $threads[$i][0];
				$title = get_translated_label($threads[$i][1], $_SESSION['lang']);
				$desc = get_translated_label($threads[$i][2], $_SESSION['lang']);
				$owner = get_player_username($threads[$i][3]);
				$icon = $threads[$i][4];
				$answers = $threads[$i][5];
				$views = $threads[$i][6];
				$publicationTime = gmdate("Y-m-d H:i:s", $threads[$i][7]);
				echo "<a href='index.php?mid=8&thread=$id'><img src=\"$icon\"<p>$title</p><p>$desc</p><p>$owner</p></a><p>$views</p><p>$publicationTime</p><br>";
			}
		}
	}*/
?>