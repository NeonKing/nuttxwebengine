<?php

	
	//////////////////////////////////////////////
	// Include Modèle
	//////////////////////////////////////////////
	
	include_once '../../includes/functions.php'; 
	
	//////////////////////////////////////////////
	/// Démarrage session
	//////////////////////////////////////////////
	
	sec_session_start();

	$to = $_POST['mail'];
	$uid=get_player_id_from_mail($to);
	$token=get_player_token($uid);
	//Lien d'accès
	$link="http://.fr/index.php?menuid=4&id=".$uid."&token=".$token;
	// En tête Mail
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: greenhopslejeu@gmail.com' . "\r\n" .
	'Reply-To: greenhopslejeu@gmail.com' . "\r\n" .
	'X-Mailer: PHP/' . phpversion(); 
	// Sujet Mail
	$subject = 'Reinitialisation du Mot de Passe de votre Compte - Green Hops';
	//Corps Mail
	$message = '<div style="min-width:70%;margin:40px;border-radius:1em;font-family:Helvetica;font-size:1.5em;color:#fff;" align="left">';
	$message = '<H1 style="font-family:Helvetica;font-size:1.5em;color:#006e2e;">R&eacute;activation de votre compte Green Hops</H1>';
	$message.= 'Merci de cliquer sur le lien suivant :<br><br>';
	$message.= '<p><a href="'.$link.'">Changer Le Mot de Passe</a></p></div>';
	//Envoi Mail
	mail($to, $subject, $message, $headers);
	//TODO Update token
	header('Location: ../index.php?val=8');
?>
