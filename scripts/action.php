<?php

	
	include_once '../../includes/functions.php';
	include_once '../../includes/greenhops.php';
	
	sec_session_start();

	include_once '../../includes/system/time_stuff.php';

	include_once 'global_vars.php';

	if ( (isset($_GET['id'])) ) 
	{ 

		//Récupération de la valeur du niveau du joueur
		$uid = $_SESSION['user_id'];
		$level=get_numeric_player_characteristic('Niveau',$uid);
		$action = intval(filter_input(INPUT_GET, 'id',  FILTER_SANITIZE_NUMBER_INT)); // Identifiant action
		$actiontime=get_action_time($action,$uid);// Sélection du temps de l'action
		//$actiontime=30; // Override de test
		
		if ($action >= 1 && $action <= 10)
		{
			$getroom=intval(filter_input(INPUT_GET, 'room',  FILTER_SANITIZE_NUMBER_INT)); // Identifiant room
			if (intval($getroom) > $level || intval($getroom) <= 0) // Initialisation défaut du numéro de room (anti override GET)
			{
				$roomnb=1;
			}
			else 
			{
				$roomnb=$getroom;
			}
			
			$room="room".$roomnb; // nom du champ bdd de l'état room
			
			// Récupération de l'état global de room
			$statet = get_room_state($roomnb,$uid);
			$rstate= $statet[0]; // Etat de la room
			$upto= $statet[1]; // timestamp de fin d'action
			$upgrade = $statet[2]; // Niveau de la room
			$dirt=$statet[3]; // Etat de salete de la roon (1-10)
			if (count($statet) == 8)
			{
				$taille = $statet[4]; // coefficient lie a la taille du plant
				$palissage = $statet[5]; // coefficient lie au palissage du plant
				$rendement = $statet[6]; // rendement de la room (5 valeurs separees par des :)
				$stepsdone = $statet[7]; // Nombre d'updates deja effectues
			}
			else if ($rstate == 'Diversion')
				$diversionType = $statet[4];
			
			$equip=get_room_equipement($roomnb, $uid); // Tableau 50 éléments représentant les index des élements équipés par slots
		}
		else if (($action==11)||($action==20))
		{
			$slot=intval(filter_input(INPUT_GET, 'slot',  FILTER_SANITIZE_NUMBER_INT));
			$brewerystatet= get_brewery_state($uid);
			$bstate = $brewerystatet[0];
			$bupto= intval($brewerystatet[1]);
			$brewequip=get_brewery_slot_state($slot,$uid);
		}
		else if ($action >= 12 && $action <= 14)
		{
			$slot=intval(filter_input(INPUT_GET, 'slot',  FILTER_SANITIZE_NUMBER_INT));
			$val=intval(filter_input(INPUT_GET, 'val',  FILTER_SANITIZE_NUMBER_INT));
			$slotstatus=get_friends_slot_state($slot,$uid);
			$hopstock=get_hop_stock($uid);//Récupération du stock houblon
		}
		
		$errid=""; // Initialisation Chaine d'erreurs
		
		/////////////////////////////////////////////
		// Traitement des actions selon leur qualité
		/////////////////////////////////////////////
		
		switch ($action)
		{
			case 1: //planter
				//checker Etat Room
				if ($rstate=='inactive') // Si inactive
				{
					//Si lamp+ballst+réflecteur ds le slot 1
					$indexlamp=0;
					$indexballast=1;
					$indexreflecteur=2;
					$indexseed = 7;
					$indexengrais = 8;
					$seedMinValue = 11;
					$fertilizerMinValue = 28;
					$seedsToPlant = array($seedMinValue => 0,0,0,0,0);
					$fertilizers = array($fertilizerMinValue => 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
					$isplantpossible = true;
					$inventory = get_player_inventory ($uid);
					// On verifie quels slots ont recu une graine, de l'engrais, et s'ils ont de quoi les faire pousser
					for ($i = 0; $i < $upgrade; $i++)
					{
						$seed = intval(filter_input(INPUT_POST, $i.'-seed',  FILTER_SANITIZE_NUMBER_INT));
						if ($seed != 0)
						{
							$seedsToPlant[$seed]++;
							if ($seed < 11 || $seed > 15
								|| !($equip[10 * $i + $indexlamp]>0)
								|| !($equip[10 * $i + $indexballast]>0)
								|| !($equip[10 * $i + $indexreflecteur]>0))
							{
								$isplantpossible = false;
							}
						}
						$fertilizer = intval(filter_input(INPUT_POST, $i.'-fertilizer',  FILTER_SANITIZE_NUMBER_INT));
						if ($fertilizer != 0)
						{	
							$fertilizers[$fertilizer]++;
							if ($fertilizer < 28 && $fertilizer > 51)
								$isplantpossible = false;
						}
					}
					// On verifie avoir suffisament de graines et d'engrais dans l'inventaire
					for ($i = $seedMinValue; $i < count($seedsToPlant) + $seedMinValue; $i++)
					{
						if ($seedsToPlant[$i] > 0 && $inventory[$i] < $seedsToPlant[$i])
							$isplantpossible = false;
					}
					for ($i = $fertilizerMinValue; $i < count($fertilizers) + $fertilizerMinValue; $i++)
					{
						if ($fertilizers[$i] > 0 && $inventory[$i] < $fertilizers[$i])
							$isplantpossible = false;
					}

					if ($isplantpossible && array_sum($seedsToPlant) > 0)
					{
						//Soustraire graines et engrais sélectionnées du stock
						for ($i = 0; $i < $upgrade; $i++)
						{
							$seed = intval(filter_input(INPUT_POST, $i.'-seed',  FILTER_SANITIZE_NUMBER_INT));
							if ($seed != 0)
							{
								if (update_item_qty($uid, $seed, -1))
								{
									$equip[10 * $i + $indexseed] = $seed;
								}
								else
								{
									$errid=26;
								}
								$fertilizer = intval(filter_input(INPUT_POST, $i.'-fertilizer',  FILTER_SANITIZE_NUMBER_INT));
								if ($fertilizer != 0)
								{
									$equip[10 * i + $indexengrais] = $fertilizer;
									if (!update_item_qty($uid, $fertilizer, -1))
										$errid=26;
								}
							}
						}
						//On rééquipe la room
						for($m=0;$m<49;$m++)
						{
							$newequip.=$equip[$m]."-";
						}
						$newequip.=$equip[49];
						// MAJ état Room
						if (!update_room_equipement($newequip, $roomnb, $uid))
							$errid=26;
						$state = "grow";
						$newupto=$now+$actiontime; // On changes le flag temporel déclencheur
						$taille = 1;
						$palissage = 1;
						$rendement = "0:0:0:0:0";
						$nb_steps = 0;
						$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt."-".$taille."-".$palissage."-".$rendement."-".$nb_steps; // Nouvel état de room
						if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
						$iid=$roomnb; // La cible du log devient la room
					}
					else
					{
						$errid="12"; // Pas d'équipement minimum
					}
				}
				else
				{
					$errid="13"; // Room occupée
				}
				break;
			case 2: //Laver
				//checker Etat Room
				if ($rstate=='inactive') // Si inactive
				{
					$state="wash"; // Nouvel état
					$newupto=$now+$actiontime; // On changes le flag temporel
					$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt; // Nouvel état de room
					if (!update_room_state($newroomstate, $roomnb,$uid))
						$error = 26;
				}
				else
				{
					$errid="14"; // On ne peux laver qu'une room inactive
				}
				break;
			case 3: //Tailler
				if ($rstate=='grow') // Si en croissance
				{
					//Calcul Delta
					$deltamin=intval($upto)-(round(get_action_time(1,$uid)*0.15))-(get_action_time(3,$uid));
					$deltamax=intval($upto)-(round(get_action_time(1,$uid)*0.15));
					if (($now>$deltamin)&&($now<$deltamax))
					{
						$state="grow";
						$newupto=$upto; // On conserves le flag temporel
						$taille = 1.1;
						$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt."-".$taille."-".$palissage."-".$rendement."-".$stepsdone; // Nouvel état de room
						if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
						$val = 11;
					}
					else
					{
						$errid="15"; // Pas ds le timing
					}
				}
				else
				{
					$errid="16"; // Il faut être en croissance
				}
				break; 
			case 4: //Floraison
				if ($rstate=='growend') // Si croissance finie
				{
					if ($now>$upto) // Vérification flag temporel
					{
						$state="bloom"; // Nouvel état 
						$newupto=$now+$actiontime; // On changes le flag temporel
						$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt."-".$taille."-".$palissage."-".$rendement."-".$stepsdone; // Nouvel état de room
						if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
					}
					else
					{
						$errid="17"; // Croissance pas terminée
					}							
				}
				else 
				{
					$errid="18"; // Il faut que la plante ai fini sa croissance pour être passée en flo
				}
				break;
			case 5: //Palissage
				if ($rstate=='bloom' || $rstate == 'blow') // Si en floraison | correction d'erreur, a enlever une fois la bdd saine
				{
					//Calcul du delta
					$deltamin=intval($upto)-(round(get_action_time(4,$uid)*0.75))-(get_action_time(5,$uid));
					$deltamax=intval($upto)-(round(get_action_time(4,$uid)*0.75));
					if (($now>$deltamin)&&($now<$deltamax) )
					{
						$palissage = 1.1;
						$newroomstate=$rstate."-".$upto."-".$upgrade."-".$dirt."-".$taille."-".$palissage."-".$rendement."-".$stepsdone;// Nouvel état de room
						if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
						$val = 12;
					}
					else
					{
						$errid="19"; // Pas dans le timing
					}
				}
				else
				{
					$errid="20"; // Le palissage se réalise uniquement en floraison
				}
				break;
			case 6: //Récolte
				if ($rstate=='bloomend')
				{
					if (!update_rendement($roomnb, 1, $uid))
						$errid = 26;
					$seedIdMin = 11;
					$seedIndex = 7;
					$StockCapacityPerLevel = 1000;
					$rendement = explode(':', $rendement);
					$initialRendement = array_sum($rendement);
					$availableStock = get_numeric_player_characteristic('StockLvl',$uid) * $StockCapacityPerLevel - array_sum(get_hop_stock($uid));
					$exces = $initialRendement - $availableStock;
					$harvested_varieties = array(0, 0, 0, 0, 0);
					$equip = get_room_equipement($roomnb,$uid);
					// On regarde quelles varietes de houblon ont ete plantes
					for ($i = 0; $i < $upgrade; $i++)
					{
						if ($equip[$seedIndex + $i * 10] != 0)
							$harvested_varieties[$equip[$seedIndex + $i * 10] - $seedIdMin] += 1;
					}
					// Si il a plus de houblon recolte que de place dans l'inventaire
					if ($exces > 0)
					{
						// On enleve l'excedant
						//$hop_loss = ceil($exces / array_sum($harvested_varieties));
						$hop_loss = $availableStock / $initialRendement;
						for ($i = 0; $i < count($rendement); $i++)
						{
							//$rendement[$i] -= $harvested_varieties[$i] * $hop_loss;
							$rendement[$i] = floor ($rendement[$i] * $hop_loss);
						}
					}
					// Mise a jour du stock de houblon
					for ($i = 0; $i < count($rendement); $i++)
					{
						if ($harvested_varieties[$i])
						{
							if (!update_hop_qty($uid, $i, $rendement[$i]))
								$errid = 26;
							// TODO faire en sorte que le log puisse montrer toutes les varietes recoltes
							log_player_success($uid, gmdate("Y-m-d H:i:s", $now), 2, $seedIdMin + $i, $rendement[$i]);
						}
					}
					// On desequipe les graines et les engrais
					$indexseed = 7;
					$indexfertilizer = 8;
					for ($i = 0; $i < $upgrade; $i++)
					{
						$equip[$indexseed + $i * 10]=0;
						$equip[$indexfertilizer + $i * 10]=0;
					}
					//On rééquipe la room
					$newequip="";
					for($m=0;$m<49;$m++)
					{
						$newequip.=$equip[$m]."-";
					}
					$newequip.=$equip[49];
					// MAJ état Room
					if (!update_room_equipement($newequip, $roomnb, $uid))
						$errid=26;
					// MAJ points
					if (!update_numeric_player_characteristic(array_sum($harvested_varieties),'points',$uid))
						$errid=26;
					// MAJ niveau si necessaire
					$newpoints=get_numeric_player_characteristic('points',$uid);
					if ($newpoints>$levelsteps[$level]) // Si le nouveau nombre de points est supérieur au pallier du niveau
					{
						//On augmentes le niveau
						if (!update_numeric_player_characteristic(1,'Niveau',$uid))
							$errid = 26;
						// Enregistrer le succes de la montee de niveau. On enregistre le niveau atteint.
						log_player_success($uid, gmdate("Y-m-d H:i:s", $now), 1, "", get_numeric_player_characteristic('Niveau',$uid));
					}
					// MaJ Etat de la room
					$state="inactive"; // remise en état inactif
					$newupto=$now; // a partir de maintenant
					$dirt=min($dirt+1, 10); // augmente le niveau de saleté
					$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt; // nouvel état de room
					if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
					// TODO contenu popup $pr=
					$pr = array_sum($harvested_varieties).'-'.$initialRendement;
					for ($i = 0; $i < count($rendement); $i++)
						$pr .= '-'.$rendement[$i];
					if ($exces > 0)
						$pr .= "-$exces";
				}
				else
				{
					$errid = 24;
				}
				break;
			case 7: //Arracher
				if ($rstate == 'grow' || $rstate == 'growend' ||$rstate == 'bloom' ||$rstate == 'bloomend')
				{
					// Enlever graines et engrais
					for ($i = 0; $i < $upgrade; $i += 10)
					{
						$equip[$i + 7] = 0;
						$equip[$i + 8] = 0;
					}
					$roomequip="";
					//On recrée la chaine d'état
					for ($k=0;$k<49;$k++)
					{
						$roomequip.=$equip[$k]."-";
					}
					$roomequip.="0";
					if (!update_room_equipement($roomequip, $roomnb,$uid))
						$error = 26;
					// Réinitialisation Room
					$state="inactive";
					$newupto=$now;
					$dirt=min($dirt+1, 10); // augmente le niveau de saleté
					$newroomstate=$state."-".$newupto."-".$upgrade."-".$dirt;
					if (!update_room_state($newroomstate, $roomnb,$uid))
						$error = 26;
				}
				break;
			case 8: // Diversion
				if ($rstate == 'inactive')
				{
					$diversion = intval(filter_input(INPUT_POST, 'diversion_choice',  FILTER_SANITIZE_NUMBER_INT));
					if ($diversion >= 0 && $diversion <= 10)
					{
						$actiontime = $diversiontimes[$diversion];
						$newupto=$now+$actiontime; // Augmentation Flag temporel
						$newroomstate="diversion-".$newupto."-".$upgrade."-".$dirt."-".$diversion; // Nouvel état de room
						if (!update_room_state($newroomstate, $roomnb,$uid))
							$error = 26;
					}
					else
					{
						$errid = 26;
					}
				}
				else
					$errid = 21;
				break;
			case 9: //Upgrader
				if (($upgrade<5)&&($upgrade>=0))
				{
					if ($rstate == 'inactive')
					{
						$cost=$upgradecost*$roomnb;
						$solde=get_numeric_player_characteristic('credit',$uid);
						if ($solde - $cost >= 0)
						{
							if (!update_numeric_player_characteristic((0-$cost),'credit',$uid))
								$errid=26;          
							if ($upgrade == 0)
								$newupto = $now;
							else
								$newupto=$now+$actiontime;
							$newroomstate="upgrade-".$newupto."-".$upgrade."-".$dirt;
							if (!update_room_state($newroomstate, $roomnb,$uid))
								$error = 26;
						}
						else
						{
							$errid = 31;
						}
					}
					else
					{
						$errid = 22;
					}
				}
				 break;
			case 10 : //Equiper
				if ($rstate=='inactive')
					equip_room($roomnb,$_POST,$uid);
				else
					$errid = 21;
				break;
			case 11: //Vendre
				//On check le niveau de stock
				$market_lvl=get_numeric_player_characteristic('MarketLvl',$uid);
				//Récupération infos slot vente
				$si=intval(filter_input(INPUT_GET, 'si',  FILTER_SANITIZE_NUMBER_INT));
				$brewequip=get_brewery_slot_state($slot,$uid);
				$bstate = $brewequip[0];
				if ($bstate=='inactive')
				{
					//Sélectionner le houblon demandé
					$hi=intval(filter_input(INPUT_GET, 'hi',  FILTER_SANITIZE_NUMBER_INT)); // index houblon
					$hopstock=get_hop_stock($uid); // Tableau stock indéxé 0
					$stockqty=$hopstock[$hi-1];
					$baseval=($basesellval[$si])*$market_lvl;
					// Si on as de quoi vendre
					if ($stockqty - $baseval >= 0)
					{
						update_hop_qty($uid,($hi-1),(0-$baseval)); //On Soustrait le houblons
						$newupto = $now + $actiontime * $market_lvl;
						$newbrewstate='active-'.$newupto."-".$brewerystatet[$hi + 1];
						update_brewery_slot_state($newbrewstate,$baseval,$slot,$uid);// MAJ info slot vente
					}
					else
					{
						$errid=30; // On est trop pauvre
					}
				}
				else
				{
					$errid=29; // Slot actif
				}
				break;
			case 12: //Prêter
				if ($slotstatus[0]=='inactive' || count($slotstatus) == 1)  //Si slot vente inactif
				{
					if ((get_numeric_player_characteristic('credit',$uid)-$valeurs_pret_remboursement[$val])>0)// Si on as de quoi preter
					{
						update_numeric_player_characteristic(-$valeurs_pret_remboursement[$val],'credit',$uid);//On retire les sous
						$bonus = ($val==4)?5:$val;
						update_numeric_player_characteristic($bonus,'social',$uid);//On augmente l'amitie
						$newslotstate='active-'.($now+($actiontime*$val))."-".$val;
						update_friends_slot_state($newslotstate,$slot,$uid);
					}
					else
					{
						$errid=34; // On est trop pauvre
					}
				}
				else
				{
					$errid=33; // Slot actif
				}
				break;
			case 13: //Emprunter
				$dette = get_numeric_player_characteristic('debit', $uid);
				if ($dette == 0)
				{
					update_numeric_player_characteristic($valeurs_pret_remboursement[$val],'credit',$uid);
					update_numeric_player_characteristic(-$valeurs_pret_remboursement[$val],'debit',$uid);
				}
				else
				{
					$errid = 35;
				}
				break;
			case 14: // Rembourser
				if (get_numeric_player_characteristic('credit',$uid) < $valeurs_pret_remboursement[$val])
				{
					$errid = 10;
				}
				else if (-get_numeric_player_characteristic('debit',$uid) < $valeurs_pret_remboursement[$val])
				{
					$errid = 32;
				}
				else
				{
					update_numeric_player_characteristic(-$valeurs_pret_remboursement[$val],'credit',$uid);
					update_numeric_player_characteristic($valeurs_pret_remboursement[$val],'debit',$uid);
				}
				break;	
			case 15: //acheter:
				$item=intval(filter_input(INPUT_GET, 'it',  FILTER_SANITIZE_NUMBER_INT));
				$idcat=intval(filter_input(INPUT_GET, 'cat',  FILTER_SANITIZE_NUMBER_INT));
				$qty=intval(filter_input(INPUT_GET, 'qty',  FILTER_SANITIZE_NUMBER_INT));
				$item_properties=get_item_properties($item);
				if ($item_properties['Niveau']<= $level)  // Si on a le niveau requis
				{
					$cost = $item_properties['Cout'] * $qty;
					// Si on peux se le permettre
					if (get_numeric_player_characteristic('credit',$uid) - $cost >= 0)
					{
						$amount=$result;
						update_numeric_player_characteristic(-$cost,'credit',$uid);// Mettre le crédit à jour
						update_item_qty($uid,$item,$qty);// Mettre l'inventaire à jour
						$val = 7;
					}
					else
						$errid= 10;
				}
				else
				{
					$errid=37;
				}
				break;
			case 16: // vérification des conditions avant demande de validation d'augmentation de niveau du stock
				if ($stock_lvl >= 0 && $stock_lvl < $stocklvlmax)
				{
					$discretion = get_numeric_player_characteristic('discretion', $uid);
					$money = get_numeric_player_characteristic('credit', $uid);
					if ($discretion >= $inventoryDiscretionUpgCost && $money >= $inventoryMoneyUpgCost * ($stocklevel + 1))
					{
						$val = 2;
					}
					else
					{
						$errid = 27;
					}
				}
				else
				{
					 $errid=23;           
				}
				break;
			case 17: // upgrade stock
				update_numeric_player_characteristic(-$inventoryDiscretionUpgCost, 'discretion', $uid);
				update_numeric_player_characteristic(-($stock_lvl * $inventoryMoneyUpgCost), 'credit', $uid);
				update_numeric_player_characteristic(1, 'StockLvl', $uid);
				$val = 3;
				break;
			case 18: //verification de la faisabilite de l'upgrade marché
				$market_lvl = get_numeric_player_characteristic('MarketLvl', $uid);
				if (($market_lvl>=0)&&($market_lvl<$marketlvlmax))
				{
					if (get_numeric_player_characteristic('discretion', $uid) >= $breweryDiscretionUpgCost
						&& get_numeric_player_characteristic('credit', $uid) >= $breweryMoneyUpgCost * $marketlevel
						&& get_numeric_player_characteristic('science', $uid) >= $breweryExpertiseUpgCost)
					{
						$val = 4;
					}
					else
						$errid = 27;
				}
				else
					 $errid=23;
				break;
			case 19: //Upgrade marche
				update_numeric_player_characteristic(-$breweryDiscretionUpgCost, 'discretion', $uid);
				update_numeric_player_characteristic(-$breweryExpertiseUpgCost, 'science', $uid);
				update_numeric_player_characteristic(-get_numeric_player_characteristic('MarketLvl', $uid) * $breweryMoneyUpgCost, 'credit', $uid);
				update_numeric_player_characteristic(1, 'MarketLvl', $uid);
				$val = 3;
				break;
			case 20: //changement brasserie
				if ($bstate=='inactive')
				{
					$newbrewerystate='change-'.($now+get_action_time(22));
					for ($i=0;$i<5;$i++)
					{
						$newbrewerystate .= "-".rand(5,10);
					}
					// On mets à jour l'état 
					update_brewery_state($newbrewerystate,$uid);
				}
				break;
			}
		}
		
		$iid = isset($iid)?$iid:0;
		if (($action>0)&&($action<10))//  Redirection Actions Room & Log
		{
			if ($errid=="")
			{
				// Log Action
				if (!log_player_action($uid, $dnow, $action, $iid)) 
				{
					$errid.='-25';            
				}
				if ($action==6)
				{
					$urlto='Location: ../../index.php?mid=1&room='.$roomnb.'&val=5&pr='.$pr;
				}
				else
				{
					if ($errid!="")
					{
						$urlto='Location: ../../index.php?mid=1&room='.$roomnb.'&errid='.$errid;
					}
					else
					{
						$urlto='Location: ../../index.php?mid=1&room='.$roomnb;
					}
				}
				if (isset($val))
					$urlto .= "&val=$val";
			}
			else
			{
				$urlto='Location: ../../index.php?mid=1&room='.$roomnb.'&errid='.$errid;
			}
			header($urlto); 
		}
		
		if (($action>9)&&($action<21))  // Redirection Autres actions
		{
			if (!log_player_action( $uid, $dnow, $action, $iid))
			{        
				$errid = 25;            
			}
			switch ($action)
			{	
				case 10:
					if ($errid=="")
					{
						$urlto2='Location: ../../index.php?mid=1&room='.$roomnb;
					}
					else
					{
						$urlto2='Location: ../../index.php?mid=1&errid='.$errid.'&room='.$roomnb;
					}
					break;
				case 11:
				case 18:
				case 19:
				case 20:
					if ($errid=="")
					{
						if (isset($val))
						{
							$urlto2="Location: ../../index.php?mid=4&val=$val";
						}
						else
						{
							$urlto2='Location: ../../index.php?mid=4';
						}
					}
					else
					{
						$urlto2='Location: ../../index.php?mid=4&errid='.$errid;
					}
					break;
				case 12:
				case 13:
				case 14:
					if ($errid=="")
					{
						$urlto2='Location: ../../index.php?mid=5';
					}
					else
					{
						$urlto2='Location: ../../index.php?mid=5&errid='.$errid;
					}
					break;
				case 15:
					if ($errid=="")
					{
						$idcat=$_GET['cat'];
						$urlto2='Location: ../../index.php?mid=2&cat='.$idcat;
						if (isset($val))
							$urlto2 .= "&val=$val&item=$item&qty=$qty";
					}
					else
					{
						$urlto2='Location: ../../index.php?mid=2&errid='.$errid.'&cat='.$idcat;
					}
					break;
				case 16:
				case 17:
					if ($errid=="")
					{
						if (isset($val))
						{
							$urlto2="Location: ../../index.php?mid=3&val=$val";
						}
						else
						{
							$urlto2='Location: ../../index.php?mid=3';
						}
					}
					else
					{
						$urlto2='Location: ../../index.php?mid=3&errid='.$errid;
					}
					break;
			}
			header($urlto2);// Redirection autres actions
		}
?>