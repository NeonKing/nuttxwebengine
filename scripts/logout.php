<?php 


	//////////////////////////////////////////////
	// Includes BDD
	//////////////////////////////////////////////
	
	include_once '../../includes/functions.php'; 
	
	//////////////////////
	// Démarrage session
	//////////////////////
	
	sec_session_start();
	
	//////////////////////
	// Infos de temps
	//////////////////////
	
	include_once '../../includes/system/time_stuff.php';
  
	$action=99;//déconnexion   
	log_player_action($_SESSION['user_id'], $dnow, $action, $_SESSION['user_id']); // Log déconnexion
	$_SESSION = array(); 
	$params = session_get_cookie_params(); 
	setcookie(session_name(),'', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]); 
	session_destroy(); 
	header('Location: ../../index.php'); //Redirection Vers la page d'acceuil
?>