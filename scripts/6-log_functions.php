<?php


/////////////////////////////////////
// 6.1 Fonction Log : Renvoies les infos Système Client
/////////////////////////////////////

function get_client_sys_data()
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `Player_Host`, `Player_OS`, `Player_IP` 
		FROM `logger.players_sysvals` 
		WHERE `ID` = {$_SESSION['sysval']}");
	return $tmp[0];
}

/////////////////////////////////////
// 6.2 Fonction Log : Log action - renvoies true si action loggée,sinon false
/////////////////////////////////////

function log_player_action($uid, $date, $action2log, $target)
{
	if (client_query_db($_SESSION['mysqli'],
	"INSERT INTO `logger.player_actions_log`
		(`ID_Sysval`, `ID_Character`, `ID_Action`, `Target_ID`, `Moment`)
		VALUES 
		({$_SESSION['sysval']}, $uid, $action2log, $target, '$date')",
	$error) == 0)
	 {
	 	return true;
	 }
	else
	{
		return false;	
	}
}

/////////////////////////////////////
// 6.3 Fonction Log : Log erreur - renvoies true si erreur loggée,sinon false
/////////////////////////////////////

function log_player_error($uid, $date, $error2log, $target)
{
	if (client_query_db($_SESSION['mysqli'],
	"INSERT INTO `logger.player_errors_log` 
		(`ID_Sysval`, `ID_Character`, `ID_Error`, `Target_Id`, `Moment`) 
		VALUES 
		({$_SESSION['sysval']}, $uid, $error2log, $target, '$date')")
	 == 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

/////////////////////////////////////
// 6.4 Fonction Log : Log success - renvoies true si succes loggé, sinon false
/////////////////////////////////////

function log_player_success($uid, $date, $Success2log, $AdditionalText='', $AdditionalFloat=0.0)
{
	if (client_query_db($_SESSION['mysqli'], 
	"INSERT INTO `logger.succes_log` 
		(`ID_Character`, `ID_Sysval`, `ID_Success`, `Moment`, `Amount`, `More`) 
		VALUES 
		($uid, {$_SESSION['sysval']}, $Success2log, '$date', $AdditionalFloat, '$AdditionalText')"
	, $error) == 0)
	{
		return true;
	}
	else
	{
		return false;
	}	
}

/////////////////////////////////////
// 6.5 Fonction Log : Recherche succes - renvoies un certain nombre d'un certain succes
/////////////////////////////////////

function get_succes_log($SuccessID, $qty = 1)
{
	$tmp = client_query_db($_SESSION['mysqli'],
	 "SELECT `ID`, `Moment`, `Img_ID`, `Amount`, `More`
		FROM `logger.succes_log`
		WHERE `ID_Success` = $SuccessID 
		AND `ID_Sysval` = {$_SESSION['sysval']}
		ORDER BY `Moment` 
		DESC LIMIT $qty");
	return is_array($tmp)?$tmp:null;
}

/////////////////////////////////////
// 6.6 Fonction Log : Renvoie l'index du sysval qui correspond aux parametres. En cree un nouveau si besoin.
/////////////////////////////////////

function get_sysval_id($uid, $ip, $nav, $host)
{
	$tmp = client_query_db($_SESSION['mysqli'], 
	"SELECT `ID` 
		FROM `logger.players_sysvals` 
		WHERE `ID_Player_Profile` = $uid 
		AND `Player_Host` = '$host' 
		AND `Player_IP` = '$ip' 
		AND `Player_OS` = '$nav'", $error);
	if (is_array($tmp))
	{
		return $tmp[0][0];
	}
	else
	{
		if (client_query_db($_SESSION['mysqli'],
		"INSERT INTO `logger.players_sysvals` 
			(`ID_Player_Profile`, `ID_Character`, `Player_Host`, `Player_OS`, `Player_IP`) 
			VALUES 
			($uid, $uid, '$host', '$nav', '$ip')", $error) == 0)
		{
			$tmp = client_query_db($_SESSION['mysqli'], 
			"SELECT `ID` 
				FROM `logger.players_sysvals` 
				WHERE `ID_Player_Profile` = $uid 
				AND `Player_Host` = '$host' 
				AND `Player_IP` = '$ip' 
				AND `Player_OS` = '$nav'", $error);
			return $tmp[0][0];
		}
		else
		{
			return null;
		}
	}
}
?>