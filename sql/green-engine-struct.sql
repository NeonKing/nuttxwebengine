--
-- MySQL Green Engine Tables Structure
-- Author : Jerome 'PsYkhAzE aka The_Architekt' KASPER
--

-- Tables dédiées au Multilingue et au contenu textuel

CREATE TABLE `i18n.lang` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `Country` varchar(3) COLLATE utf16_bin NOT NULL COMMENT 'Pays language (ShortName)',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Name` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom langue en language Natif',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Img`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des languages';

CREATE TABLE `i18n.translation_text` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(2) unsigned NOT NULL COMMENT 'Référence Langue',
  `ID_I18n_Regroup` int(32) unsigned NOT NULL COMMENT 'Clé de regroupement par concept',
  `T_Val` varchar(2048) COLLATE utf16_bin DEFAULT NULL COMMENT 'Valeur textuelle intitulé',
  `Last_Update` varchar(20) COLLATE utf16_bin DEFAULT NULL COMMENT 'dernière date MAJ',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des intitulés Multilingues i18n';

CREATE TABLE `i18n.articles` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(32) unsigned NOT NULL COMMENT 'Reference a la langue de rédaction du bloc',
  `ID_Menu` int(32) unsigned DEFAULT NULL COMMENT 'Reference Routing',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement/thread parent',
  `ID_Author` int(32) unsigned DEFAULT NULL COMMENT 'Référence Rédacteur Bloc',
  `ID_Icon` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Title_Lib` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Reference concept i18n titre bloc texte',
  `T_Val` varchar(8196) COLLATE utf16_bin DEFAULT NULL COMMENT 'Contenu du bloc de texte',
  `Publication_Moment` varchar(20) COLLATE utf16_bin DEFAULT NULL COMMENT 'Timestamp Publication',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`,`ID_Menu`,`ID_Parent_Env`,`ID_Author`,`ID_Icon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table blocs textuels';

CREATE TABLE `i18n.helpers` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(2) unsigned NOT NULL COMMENT 'Référence Langue',
  `ID_Helper_Group` int(32) unsigned DEFAULT NULL COMMENT 'Clé de regroupement par concept',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence ID I18N',
  PRIMARY KEY (`ID`),
  INDEX (`ID_Lang`,`ID_Helper_Group`,`ID_Parent_Env`,`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des languages';

-- Tables Système

CREATE TABLE `sys.env_types`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `EnvType` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom',
  PRIMARY KEY (`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Types d''Environnements de Jeu';

CREATE TABLE `sys.paths`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `PathName` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Valeur du chemin (fin par /)',
  PRIMARY KEY (`ID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des chemins de l''Arborescence';
 
 CREATE TABLE `sys.subparts` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `PartName` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom de la sous-partie',
  PRIMARY KEY (`ID`)
)ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des sous-parties Applicatives';

CREATE TABLE `sys.authors` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique', 
  `FirstName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Prénom', 
  `LastName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom', 
  `Nick` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Pseudo',
  `Account` int(32) unsigned DEFAULT NULL COMMENT 'Référence Data Player',
  PRIMARY KEY (`ID`),
  INDEX(`Account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Auteurs';

CREATE TABLE `sys.imgs_types`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `PicExtension` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom de l''Extension',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Types de Ressources graphiques';

CREATE TABLE `sys.target_types`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `Target` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom de la Cible',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Types de cibles';

CREATE TABLE `sys.trigger_types`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `Target` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom du déclencheur',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Types de Déclencheurs';

CREATE TABLE `sys.param_files`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `FileName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom du fichier',
  `PathName` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chemin du fichier',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Fichiers de paramètres';

CREATE TABLE `sys.global_game_settings`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement si il y a lieu',
  `T_Val` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Valeur Texte',
  `N_Val` float(32,2) DEFAULT NULL COMMENT 'Valeur numérique courante',
  `A_Val` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Valeur Tableau en splitted str',
  `Param_Group` int(32) unsigned DEFAULT NULL COMMENT 'Reference Groupement paramètres',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Nom',
  PRIMARY KEY (`ID`),
  INDEX (`ID_Env`,`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Paramètres généraux de Jeu';

CREATE TABLE `sys.imgs`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `FileName` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom du fichier',
  `Path_ID` int(32) unsigned NOT NULL COMMENT 'Référence au chemin',
  `Drawer_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Dessinateur',
  `Author_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Auteur',
  `Subpart_ID` int(32) unsigned DEFAULT NULL DEFAULT NULL COMMENT 'Référence à la sous-partie',
  `ImgType_ID` int(32) unsigned DEFAULT NULL COMMENT 'Type d''Extension Fichier',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`Path_ID`,`Drawer_ID`,`Author_ID`,`Subpart_ID`,`ImgType_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Ressources graphiques';

CREATE TABLE `sys.php_modules`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `FileName` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom du fichier',
  `Path_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence au chemin',
  `Subpart_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence à la sous-partie',
  `AppName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom de l''application',
  `Param_Group` int(32) unsigned DEFAULT NULL COMMENT 'Groupe de Paramètres Associés',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`Path_ID`,`Subpart_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Modules PHP';

CREATE TABLE `sys.js_libs`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `FileName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom du fichier',
  `Path_ID`  int(32) unsigned NOT NULL COMMENT 'Référence au chemin',
  `Subpart_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence à la sous-partie',
  `DocLink` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Lien vers la Documentation',
  `Style_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence au style css associé',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`Path_ID`,`Subpart_ID`,`Style_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Modules Javascript';

CREATE TABLE `sys.css_styles`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `FileName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom du fichier',
  `Path_ID` int(32) unsigned NOT NULL COMMENT 'Référence au chemin',
  `Author_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Auteur',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`,`Path_ID`,`Author_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Styles CSS Vues/Modules';

CREATE TABLE `sys.environments` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Parent` int(32) unsigned DEFAULT NULL COMMENT 'Référence autochainée pour hiérarchisation',
  `ID_Menu` int(32) unsigned DEFAULT NULL COMMENT 'Reference Routing',
  `ID_Icon` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Module_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Controleur php associé',
  `Style_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence au style css associé',
  `JS_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence au module javascript associé',
  `Env_Type` int(32) unsigned DEFAULT NULL COMMENT 'Portee du contexte',
  `I18n_Label_ID` int(32) unsigned NOT NULL COMMENT 'Nom i18n',
  `Link` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Lien',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Menu`,`ID_Icon`,`Module_ID`,`Style_ID`,`JS_ID`,`Env_Type`,`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Environnements de Jeu';

-- Tables Green Hops

CREATE TABLE `gh.gifts`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(32) unsigned NOT NULL COMMENT 'Langue du cadeau',
  `Place_min` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Place mini',
  `Place_max` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Place max',
  `I18n_Label` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom du cadeau',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Cadeaux';

CREATE TABLE `gh.shops` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(2) unsigned NOT NULL COMMENT 'Référence langue shop',
  `I18n_Description_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence intitulé description shop',
  `ID_Trend` int(32) unsigned DEFAULT NULL COMMENT 'Promoteur propriétaire magasin',
  `Name` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom du shop',
  `Dpt` varchar(2) COLLATE utf16_bin NOT NULL COMMENT 'Département',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`,`I18n_Description_ID`,`ID_Trend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Liste des shops référencés';

CREATE TABLE `gh.trendz` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Intitulé i18n promoteur',
  `ID_Lang` int(32) unsigned DEFAULT NULL COMMENT 'Référence Pays promoteur',
  `Logo_Path` int(32) unsigned DEFAULT NULL COMMENT 'Référence image logo',
  `Name` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom',
  PRIMARY KEY (`ID`),
  INDEX(`I18n_Label_ID`,`ID_Lang`,`Logo_Path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Liste des promoteurs';

CREATE TABLE `gh.character_specs_types` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique', 
  `I18n_Label_ID` int(32) unsigned NOT NULL COMMENT 'Référence Label i18n',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Min_Val` int(128) DEFAULT NULL COMMENT 'Debut du spectre de valeur',
  `Max_Val` int(128) DEFAULT NULL COMMENT 'Fin du spectre de valeur',
  `Name` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom',
  PRIMARY KEY (`ID`),
  INDEX(`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des types de caracteristiques personnage';

CREATE TABLE `gh.character_specs` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Description_i18n` int(32) unsigned DEFAULT NULL COMMENT 'Référence Description i18n',
  `ID_Character` int(32) unsigned DEFAULT NULL COMMENT 'Identifiant personnage',
  `ID_Owner` int(32) unsigned NOT NULL COMMENT 'Identifiant propriétaire',
  `Niveau` int(32) unsigned NOT NULL COMMENT 'Niveau',
  `points` int(32) unsigned NOT NULL COMMENT 'points',
  `science` int(32) unsigned NOT NULL COMMENT 'points science',
  `social` int(32) unsigned NOT NULL COMMENT 'points social',
  `discretion` int(32) unsigned NOT NULL COMMENT 'discretion',
  `debit` int(32) NOT NULL COMMENT 'dette',
  `credit` int(32) unsigned NOT NULL COMMENT 'credit',
  `MarketLvl` int(32) unsigned NOT NULL COMMENT 'Niveau marché',
  `StockLvl` int(32) unsigned NOT NULL COMMENT 'Niveau stock',
  `Houblonstock` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Chaine à explode de stock houblon',
  `Inventory`  varchar(512) COLLATE utf16_bin NOT NULL COMMENT 'Chaine à explode d''Inventaire',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Description_i18n`,`ID_Character`,`ID_Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des caracteristiques personnage';

CREATE TABLE `gh.env_specs` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Env` int(32) unsigned DEFAULT NULL COMMENT 'Reference contexte jeu',
  `ID_Owner` int(32) unsigned NOT NULL COMMENT 'Propriétaire',
  `Room_State1` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 1',
  `Room_Equip1` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 1',
  `Room_State2` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 2',
  `Room_Equip2` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 2',
  `Room_State3` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 3',
  `Room_Equip3` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 3',
  `Room_State4` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 4',
  `Room_Equip4` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 4',
  `Room_State5` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 5',
  `Room_Equip5` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 5',
  `Room_State6` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 6',
  `Room_Equip6` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 6',
  `Room_State7` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 7',
  `Room_Equip7` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 7',
  `Room_State8` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 8',
  `Room_Equip8` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 8',
  `Room_State9` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 9',
  `Room_Equip9` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 9',
  `Room_State10` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 10',
  `Room_Equip10` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 10',
  `Room_State11` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 11',
  `Room_Equip11` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 11',
  `Room_State12` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 12',
  `Room_Equip12` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 12',
  `Room_State13` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 13',
  `Room_Equip13` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 13',
  `Room_State14` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 14',
  `Room_Equip14` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 14',
  `Room_State15` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 15',
  `Room_Equip15` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Roomironnement 15',
  `Room_State16` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 16',
  `Room_Equip16` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine à explode d''Equipement Environnement 16',
  `Brewery_Slot_State1` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 17',
  `Brewery_Slot_Val1` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Brewery_Slotironnement 17',
  `Brewery_Slot_State2` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 18',
  `Brewery_Slot_Val2` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Brewery_Slotironnement 18',
  `Brewery_Slot_State3` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 19',
  `Brewery_Slot_Val3` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Brewery_Slotironnement 19',
  `Brewery_Slot_State4` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 20',
  `Brewery_Slot_Val4` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Brewery_Slotironnement 20',
  `Brewery_Slot_State5` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 21',
  `Brewery_Slot_Val5` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Environnement 21',
  `Brewery_State` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 22',
  `Friend_Slot_State1` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 23',
  `Friend_Slot_Val1` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Friend_Slotironnement 23',
  `Friend_Slot_State2` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 24',
  `Friend_Slot_Val2` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Friend_Slotironnement 24',
  `Friend_Slot_State3` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 25',
  `Friend_Slot_Val3` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Friend_Slotironnement 25',
  `Friend_Slot_State4` varchar(128) COLLATE utf16_bin DEFAULT NULL COMMENT 'Chaine d''Etat 26',
  `Friend_Slot_Val4` float(16,2) DEFAULT NULL COMMENT 'Valeur numérique Environnement 26',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Env`,`ID_Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des caractéristiques Contexte de jeu';

CREATE TABLE `gh.fields_mapping` (
	`ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
	`Mapping` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Champs des tables dont le referencement est necessaire',
	PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table de mapping pour env_specs et character_specs';

-- Tables dédiées a la gestion d'events

CREATE TABLE `ev.actions` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Action` int(32) unsigned NOT NULL COMMENT 'Id Cible',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien Environnement parent',
  `Cycled` tinyint(1) DEFAULT NULL COMMENT 'Cyclique',
  `Cycle_s` int(32) unsigned DEFAULT NULL COMMENT 'Duree Action s',
  `ID_TargetType` int(32) unsigned DEFAULT NULL COMMENT 'Référence Cible',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Label I18N',
  `TriggerType` int(32) unsigned DEFAULT NULL COMMENT 'Type Déclencheur',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`,`ID_TargetType`,`I18n_Label_ID`,`TriggerType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Actions ciblees possibles';

CREATE TABLE `ev.errors` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Error` int(32) unsigned NOT NULL COMMENT 'Id Erreur',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `TargetType`  int(32) unsigned NOT NULL COMMENT 'Référence Cible',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Label I18N',
  `ErrTriggerType` int(32) unsigned DEFAULT NULL COMMENT 'Type Déclencheur',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`,`TargetType`,`I18n_Label_ID`,`ErrTriggerType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Erreurs ciblées possibles';

CREATE TABLE `ev.states` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
  `ID_PreState` int(32) unsigned DEFAULT NULL COMMENT 'Prédecesseur Etat',
  `ID_Action` int(32) unsigned DEFAULT NULL COMMENT 'Prédecesseur Action',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Cycled` tinyint(1) DEFAULT NULL COMMENT 'Cyclique',
  `Cycle_s` int(32) unsigned DEFAULT NULL COMMENT 'Durée du cycle si cyclique',
  `Regroup` int(10) unsigned DEFAULT NULL COMMENT 'Index de regroupement pour les variations',
  `Target_Type`  int(32) unsigned NOT NULL COMMENT 'Référence Cible',
  `Trigger_type` int(32) unsigned DEFAULT NULL COMMENT 'Type de déclencheur',
  `StateName` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Nom',
  `Color` varchar(6) COLLATE utf16_bin DEFAULT NULL COMMENT 'Couleur Etat',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Action`,`ID_Img`,`Target_Type`,`Trigger_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Etats possibles';

CREATE TABLE `ev.success` (
  `ID` int(32) NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
  `ID_PreState` int(32) unsigned DEFAULT NULL COMMENT 'Prédecesseur Etat',
  `ID_PreAction` int(32) unsigned DEFAULT NULL COMMENT 'Prédecesseur Action',
  `ID_Description_I18n` int(32) unsigned DEFAULT NULL COMMENT 'Référence Description i18n',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `ID_Gift` int(32) unsigned DEFAULT NULL COMMENT 'Lien Cadeau Associé',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Nom i18n',
  `Char_spec_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Charactéristique à évaluer',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_PreState`,`ID_PreAction`,`ID_Description_I18n`,`ID_Parent_Env`,`ID_Gift`,`ID_Img`,`I18n_Label_ID`,`Char_spec_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Succès possibles';

CREATE TABLE `ev.validations` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Validation` int(32) unsigned NOT NULL COMMENT 'Id Validation',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Modification induite par action',
  `TargetType` int(32) unsigned NOT NULL COMMENT 'Référence Cible',
  `ValTriggerType` int(32) unsigned DEFAULT NULL COMMENT 'Type Déclencheur',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`,`I18n_Label_ID`,`TargetType`,`ValTriggerType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Validations ciblées possibles';

-- Tables de données joueurs

CREATE TABLE `pd.player_profile` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `Email` char(128) COLLATE utf16_bin NOT NULL COMMENT 'Email du joueur',
  `ID_Lang` int(2) unsigned NOT NULL COMMENT 'Identifiant langage',
  `Player_Type` enum('Player','Admin','Trend') COLLATE utf16_bin NOT NULL COMMENT 'Type de joueur',
  `Active` tinyint(1) NOT NULL COMMENT 'Indicateur de blocage',
  `Username` varchar(128) COLLATE utf16_bin NOT NULL COMMENT 'Pseudo du joueur',
  `Shop` int(3) NOT NULL COMMENT 'Shop Rattachement',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des profils joueurs';

CREATE TABLE `pd.player_data` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Player_Profile` int(32) unsigned NOT NULL COMMENT 'Reference profil joueur',
  `KPass` varchar(128) COLLATE utf16_bin NOT NULL COMMENT 'pass crypte',
  `Last_Connexion` varchar(20) DEFAULT NULL COMMENT 'Timestamp de la derniere connexion',
  `Inscription_Date` varchar(20) NOT NULL COMMENT 'Timestamp d''inscription',
  `Connexion_Number` int(32) DEFAULT NULL COMMENT 'Nombre de connexion',
  `salt` varchar(128) COLLATE utf16_bin NOT NULL COMMENT 'Sel de hachage',
  `token` varchar(128) COLLATE utf16_bin NOT NULL COMMENT 'Jeton de sécurité',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Player_Profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table de données joueur';

CREATE TABLE `gp.character_profile` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Player` int(32) unsigned NOT NULL COMMENT 'Reference au joueur',
  `Nick` char(254) COLLATE utf16_bin NOT NULL COMMENT 'Pseudo joueur',
  `Avatar` varchar(254) COLLATE utf16_bin COMMENT 'Chaine Composition Avatar',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Player`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des profils personnage';

-- Tables dédiées au Log

CREATE TABLE `logger.players_sysvals` (
  `ID` int(64) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire',
  `ID_Player_Profile` int(32) unsigned NOT NULL COMMENT 'Reference joueur',
  `ID_Character` int(32) unsigned NOT NULL COMMENT 'Reference personnage',
  `Player_Host` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Machine Joueur',
  `Player_OS` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'OS Joueur',
  `Player_IP` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'IP Joueur',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Log des Valeurs système joueurs';

CREATE TABLE `logger.player_actions_log` (
  `ID` int(64) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire',
  `ID_Sysval` int(32) unsigned NOT NULL COMMENT 'Reference sysval',
  `ID_Character` int(32) unsigned NOT NULL COMMENT 'Reference personnage',
  `ID_Action` int(32) unsigned NOT NULL COMMENT 'Reference Action',
  `Target_ID` int(32) unsigned DEFAULT NULL COMMENT 'Reference cible',
  `Target_Type` int(32) unsigned DEFAULT NULL COMMENT 'Reference Type cible',
  `Moment` varchar(20) COLLATE utf16_bin NOT NULL COMMENT 'Timestamp action',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Sysval`,`ID_Character`,`ID_Action`,`Target_ID`,`Target_Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Log des actions joueur';

CREATE TABLE `logger.player_errors_log` (
  `ID` int(64) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Cle primaire',
  `ID_Sysval` int(32) unsigned NOT NULL COMMENT 'Reference sysval',
  `ID_Character` int(32) unsigned NOT NULL COMMENT 'Reference personnage',
  `ID_Error` int(32) unsigned NOT NULL COMMENT 'Reference Action',
  `Target_ID` int(32) unsigned DEFAULT NULL COMMENT 'Reference cible',
  `Target_Type` int(32) unsigned DEFAULT NULL COMMENT 'Reference Type cible',
  `Moment` varchar(20) COLLATE utf16_bin NOT NULL COMMENT 'Timestamp action',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Sysval`,`ID_Character`,`ID_Error`,`Target_ID`,`Target_Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Log des erreurs joueur';

CREATE TABLE `logger.succes_log` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Character` int(32) unsigned NOT NULL COMMENT 'ID du joueur a qui appartient le succes',
  `ID_Sysval` int(32) unsigned NOT NULL COMMENT 'Reference sysval',
  `ID_Success` int(32) unsigned NOT NULL COMMENT 'Référence du succès',
  `Moment` varchar(20) COLLATE utf16_bin DEFAULT NULL COMMENT 'Timestamp action',
  `Img_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Icone',
  `Amount` float(16,2) NULL COMMENT 'Montant lié  au succès',
  `More` varchar(2096) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Sysval`,`ID_Success`,`Img_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Log des succès';

-- Tables Plugins Système

CREATE TABLE `plugin.quizz` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Trend` int(32) unsigned NOT NULL COMMENT 'Reference promoteur',
  `ID_Lang` int(32) unsigned DEFAULT NULL COMMENT 'Reference langue redaction quizz',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Trend`,`ID_Lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des quizz';

CREATE TABLE `plugin.questions` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Quizz` int(32) unsigned NOT NULL COMMENT 'Reference quizz',
  `I18n_Question_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Question i18n',
  `True_Answer_ID` int(32) unsigned DEFAULT NULL COMMENT 'Réponse juste i18n',
  `Fake_answer_1_ID` int(32) unsigned DEFAULT NULL COMMENT 'fausse réponse 1 i18n',
  `Fake_answer_2_ID` int(32) unsigned DEFAULT NULL COMMENT 'fausse réponse 2 i18n',
  `Fake_answer_3_ID` int(32) unsigned DEFAULT NULL COMMENT 'fausse réponse 3 i18n',
  `Fake_answer_4_ID` int(32) unsigned DEFAULT NULL COMMENT 'fausse réponse 4 i18n',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Quizz`,`I18n_Question_Label_ID`,`True_Answer_ID`,`Fake_answer_1_ID`,`Fake_answer_2_ID`,`Fake_answer_3_ID`,`Fake_answer_4_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table de questions des quizz';

CREATE TABLE `plugin.forum_threads` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(32) unsigned DEFAULT NULL COMMENT 'Reference linguistique thread',
  `I18n_Title_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Titre i18n',
  `I18n_Decription_ID` int(32) unsigned DEFAULT NULL COMMENT 'Référence Description i18n',
  `ID_Parent` int(32) unsigned DEFAULT NULL COMMENT 'Reference Autochainée',
  `ID_Owner` int(32) unsigned DEFAULT NULL COMMENT 'Propriétaire',
  `ID_Icon` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `Thread_nb` int(32) unsigned DEFAULT NULL COMMENT 'Nombre d''Enfants',
  `Answers` int(32) unsigned DEFAULT NULL COMMENT 'Nombre de réponses au thread',
  `Publication_Moment` varchar(20) COLLATE utf16_bin DEFAULT NULL COMMENT 'Timestamp Publication',
  `Validated` tinyint(1) DEFAULT NULL COMMENT 'Etat de publication',
  `More` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`,`I18n_Title_ID`,`I18n_Decription_ID`,`ID_Parent`,`ID_Owner`,`ID_Icon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table de l''Arborescence Forum';

CREATE TABLE `plugin.shoutbox` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Lang` int(32) unsigned DEFAULT NULL COMMENT 'Reference linguistique thread',
  `ID_Owner` int(32) unsigned DEFAULT NULL COMMENT 'Propriétaire',
  `Publication_MomentPublication_Moment` varchar(20) COLLATE utf16_bin DEFAULT NULL COMMENT 'Timestamp Publication',
  `Text` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Contenu du Shout',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Lang`,`ID_Owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table de la shoutbox';

CREATE TABLE `plugin.pnj` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Parent_Env` int(32) unsigned DEFAULT NULL COMMENT 'Lien environnement parent',
  `ID_Helper_group` int(32) unsigned DEFAULT NULL COMMENT 'Lien Groupe Helper Associé',
  `ID_Img` int(32) unsigned NOT NULL COMMENT 'Lien Ressource Graphique',
  `Name` varchar(254) COLLATE utf16_bin NOT NULL COMMENT 'Nom',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Env`,`ID_Helper_group`,`ID_Img`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

-- Tables GamePlay

CREATE TABLE `gp.avatarz_element_cats`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique', 
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Nom i18n',
  PRIMARY KEY (`ID`),
  INDEX(`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Catégories Elements avatars';

CREATE TABLE `gp.avatarz_elements` (
  `ID` int(32) NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `ID_Cat` int(32) unsigned DEFAULT NULL COMMENT 'Reference Catégorie',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Img`,`ID_Cat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Elements avatars';

CREATE TABLE  `gp.roleplay_stories_prestory`(
  `ID` int(32) NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
  `I18n_Label_ID` int(32) unsigned  DEFAULT NULL COMMENT 'valeur i18n',
  PRIMARY KEY (`ID`),
  INDEX(`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Pré-Histoires RolePlay';

CREATE TABLE `gp.roleplay_stories` (
  `ID` int(32) NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire',
  `ID_lang` int(32) unsigned NOT NULL COMMENT 'Langue de l''intitulé',
  `ID_env` int(32) unsigned DEFAULT NULL COMMENT 'Index d''environnement parent',
  `PreStory` int(32) COLLATE utf16_bin NOT NULL COMMENT 'Index du pré rp i18n',
  `I18n_Label_ID` int(32) unsigned  DEFAULT NULL COMMENT 'Texte post tirage i18n',
  `Variation` int(32) DEFAULT NULL COMMENT 'Modificateur de valeur. Pourcentage',
  `Stat` int(32) unsigned DEFAULT NULL COMMENT '0 = expertise, 1 = amitie, 2 = discretion',
  `Positif` int(32) unsigned DEFAULT NULL COMMENT '0 = négatif, 1 = positif',
  `ID_scenario` int(32) unsigned DEFAULT NULL COMMENT 'groupement de scenario',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_lang`,`ID_env`,`I18n_Label_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Histoires RolePlay';

CREATE TABLE `gp.game_elements_properties`(
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique', 
  `I18n_Label_ID` int(32) unsigned NOT NULL COMMENT 'Référence Label i18n', 
  `ID_Helper` int(32) unsigned DEFAULT NULL COMMENT 'Référence Aide',
  PRIMARY KEY (`ID`),
  INDEX(`I18n_Label_ID`,`ID_Helper`)
)ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Propriétés Elements Jeu';

CREATE TABLE `gp.game_elements_cats` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Lien Ressource Graphique',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Nom',
  `ID_Helper` int(32) unsigned DEFAULT NULL COMMENT 'Lien Aide Associée',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Img`,`I18n_Label_ID`,`ID_Helper`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table des Catégories d''Elements du jeu';

CREATE TABLE `gp.game_elements_specs` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Parent_Element` int(32) unsigned NOT NULL COMMENT 'Reférence élement Parent',
  `N_Val` float(32,2) DEFAULT NULL COMMENT 'Valeur numérique',
  `T_Val` varchar(254) COLLATE utf16_bin DEFAULT NULL COMMENT 'Valeur textuelle',
  `A_Val` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Valeur Tableau en splitted str',
  `ID_Property` int(32) unsigned NOT NULL COMMENT 'Index de la propriete',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Parent_Element`,`ID_Property`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Caractéristiques Elements Jeu';

CREATE TABLE `gp.game_elements` (
  `ID` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Clé unique',
  `ID_Env` int(32) unsigned DEFAULT NULL COMMENT 'Reference Environnement Parent',
  `ID_Trend` int(32) unsigned DEFAULT NULL COMMENT 'Reference promoteur',
  `ID_Img` int(32) unsigned DEFAULT NULL COMMENT 'Reference image',
  `I18n_Label_ID` int(32) unsigned DEFAULT NULL COMMENT 'Nom i18n',
  `ID_Cat` int(32) unsigned DEFAULT NULL COMMENT 'Reference Catégorie',
  `More` varchar(1024) COLLATE utf16_bin DEFAULT NULL COMMENT 'Infos supplémentaires',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Env`,`ID_Trend`,`ID_Img`,`I18n_Label_ID`,`ID_Cat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin COMMENT='Table Elements du jeu';
