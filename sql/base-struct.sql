﻿
--     Author : 	 Jerome 'NeonKing' KASPER
--     File :   	 Base-Struct.sql
--     Version : 	 0.10
--     Description : Base Data Model for the engine
--

--
-- System Tables
--

CREATE TABLE `sys.menu` (
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `ID_Page` 		int(8) unsigned DEFAULT NULL				COMMENT 'Referenced page to load - null as unactive link',
  `ID_Parent`		int(8) unsigned DEFAULT NULL				COMMENT 'Parent reference - null for root',
  `NavLevel` 		int(2) unsigned DEFAULT NULL 				COMMENT 'Imbrication level = Levels from the root',
  `Label`			int(8) unsigned DEFAULT NULL 				COMMENT 'Menu Label',
  `ID_Icon` 		int(12) unsigned DEFAULT NULL 				COMMENT 'Icon File reference',
  `Link` 			varchar(254) COLLATE utf8_bin DEFAULT '#' 	COMMENT 'Absolute link',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Web Menu Tree';

CREATE TABLE `sys.page` (
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `Name` 			varchar(64) COLLATE utf8_bin NOT NULL 		COMMENT 'Nom du Modèle',
  `AreaNb` 			int(3) unsigned NOT NULL 					COMMENT 'Total number of zones',
  `Content` 		varchar(512) COLLATE utf8_bin NOT NULL 		COMMENT ' : Concatened Areas ID',
  `JSContent` 		varchar(512) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'General JS Content',
  `JSFileID` 		int(8) unsigned DEFAULT NULL		 		COMMENT 'External JS file Reference',
  `CSSContent` 		varchar(512) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'General CSS Rules',
  `CSSFileID` 		int(8) unsigned DEFAULT NULL				COMMENT 'External CSS file Reference',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Web Pages Containers';

CREATE TABLE `sys.ui_areas`(
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `AreaName` 		varchar(64) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Area Name',
  `HTMLContent` 	varchar(1024) COLLATE utf8_bin DEFAULT NULL COMMENT 'Area HTML Content',
  `HTMLFileID` 		int(8) unsigned DEFAULT NULL 				COMMENT 'External HTML file Reference',
  `CSSContent` 		varchar(512) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Area CSS Rules',
  `CSSFileID` 		int(8) unsigned DEFAULT NULL 				COMMENT 'External CSS file Reference',
  `JSContent` 		varchar(512) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Area Javascript Content',
  `JSFileID` 		varchar(254) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'External JS file Reference',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Web Page Content';

CREATE TABLE `sys.imgs`(
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `ID_Page` 		int(4) unsigned DEFAULT NULL 				COMMENT 'Page Reference',
  `ID_Uploader` 	int(8) unsigned DEFAULT NULL				COMMENT 'Reference to Uploader User',
  `FileName` 		varchar(254) COLLATE utf8_bin NOT NULL 		COMMENT 'File Name',
  `PathName` 		varchar(254) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Path to file',
  `Mime` 			varchar(64) COLLATE utf8_bin NOT NULL 		COMMENT 'Mime-Type',
  `More` 			varchar(128) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Additional Infos',
  INDEX(`ID_App`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Pictures Ressources';

CREATE TABLE `sys.files`(
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `ID_Uploader` 	int(4) unsigned DEFAULT NULL 				COMMENT 'Reference to Uploader User',
  `FileName` 		varchar(254) COLLATE utf8_bin NOT NULL 		COMMENT 'File Name',
  `PathName` 		varchar(254) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Path to file',
  `Mime` 			varchar(64) COLLATE utf8_bin NOT NULL 		COMMENT 'Mime-Type',
  `More` 			varchar(128) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Additional Infos',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Other Files ressources';

CREATE TABLE `sys.links`(
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary Key',
  `ID_Uploader` 	int(8) unsigned DEFAULT NULL				COMMENT 'Reference to User - null as root',
  `Extern` 			tinyint(1) DEFAULT NULL 					COMMENT 'In-site link Flag',
  `Link` 			varchar(1024) COLLATE utf8_bin NOT NULL 	COMMENT 'Absolute URL',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Other Files ressources';

--
-- Auth / User Tables
--

CREATE TABLE `sys.user_profile` (
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Primary key',
  `Email` 			varchar(254) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'User Email',
  `User_Type` 		enum('Root','Admin','Committer','User') COLLATE utf8_bin NOT NULL 
																COMMENT 'User Type',
  `Active` 			tinyint(1) NOT NULL 						COMMENT 'Activation flag',
  `Username` 		varchar(128) COLLATE utf8_bin NOT NULL 		COMMENT 'Nickname', 
  `Hometown`		varchar(128) COLLATE utf8_bin NOT NULL 		COMMENT 'Hometown',
  `Biography`		varchar(512) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Bio',
  `Signature`		varchar(254) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Signature',
  `ID_Avatar` 		int(8) unsigned NOT NULL 					COMMENT 'Reference to Avatar Picture',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Avatar`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table des profils utilisateur';

CREATE TABLE `sys.user_data` (
  `ID` 					int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `ID_User` 			int(8) unsigned NOT NULL 				COMMENT 'User Reference',
  `KPass` 				varchar(128) COLLATE utf8_bin NOT NULL 	COMMENT 'Crypted Pass',
  `Last_Connexion` 		varchar(20) DEFAULT NULL 				COMMENT 'Last Connexion Timestamp',
  `Inscription_Date` 	varchar(20) NOT NULL 					COMMENT 'Inscription Timestamp',
  `Connexion_Number` 	int(32) DEFAULT NULL 					COMMENT 'Connexion Number',
  `Salt` 				varchar(128) COLLATE utf8_bin NOT NULL 	COMMENT 'Hash Salt',
  `Token` 				varchar(128) COLLATE utf8_bin NOT NULL 	COMMENT 'Security Token',
  PRIMARY KEY (`ID`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table de données utilisateur';

--
--  Content Management System
--

CREATE TABLE `sys.webcontent`(
  `ID` 				int(16) unsigned NOT NULL AUTO_INCREMENT 		COMMENT 'Primary key',
  `ID_Page` 		int(16) unsigned DEFAULT NULL 					COMMENT 'Parent Page Reference',
  `ID_Author` 		int(8) unsigned DEFAULT NULL 					COMMENT 'Author User Reference - null for root',
  `ID_Icon` 		int(32) unsigned DEFAULT NULL 					COMMENT 'Icon Link',
  `Title_Lib` 		varchar(1024) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Block Title',
  `HTMLContent` 	varchar(4096) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Block HTML Content',
  `PublicationStmp` varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Publication Timestamp',
  `LastEditStmp` 	varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Last Mofified Timestamp',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Author`,`ID_Icon`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table Gestion Articles Web';


--
-- Groupware
--

CREATE TABLE `plan.projects`(
  `ID` 					int(16) unsigned NOT NULL AUTO_INCREMENT 		COMMENT 'Clé primaire',
  `ID_Manager` 			int(8) unsigned DEFAULT NULL 					COMMENT 'Référence Gérant projet',
  `Project_Sublevel` 	int(3) unsigned DEFAULT NULL 					COMMENT 'Niveau d imbrication projet',
  `ID_I18n_Label` 		int(32) unsigned NOT NULL 						COMMENT 'Nom i18n',
  `Start_Moment` 		varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Timestamp départ',
  `End_Moment` 			varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Timestamp fin',
  `Status` 				varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Etat projet',
  `Project_Description` varchar(1024) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Description Projet',
  `Done` 				tinyint(1) DEFAULT NULL 						COMMENT 'Flag archivage',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Manager`,`ID_I18n_Label` )
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table des Projets';

CREATE TABLE `plan.tasks`(
  `ID` 					int(32) unsigned NOT NULL AUTO_INCREMENT 		COMMENT 'Clé primaire',
  `ID_Manager` 			int(8) unsigned NOT NULL 						COMMENT 'Référence créateur',
  `ID_Project` 			int(16) unsigned NOT NULL 						COMMENT 'Référence Projet parent',
  `Task_Parent` 		int(32) unsigned DEFAULT NULL 					COMMENT 'Référence tache parente',
  `Label` 				int(32) unsigned NOT NULL 						COMMENT 'Nom',
  `Task_Sublevel` 		int(3) unsigned DEFAULT NULL 					COMMENT 'Niveau d imbrication tache',
  `Start_Moment` 		varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Timestamp départ',
  `End_Moment` 			varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Timestamp fin',
  `Status` 				varchar(20) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Etat tache',
  `Task_Description` 	varchar(512) COLLATE utf8_bin DEFAULT NULL 		COMMENT 'Description Tache',
  `Done` 				tinyint(1) DEFAULT NULL 						COMMENT 'Flag archivage',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Manager`,`ID_Project`,`ID_I18n_Label` )
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table des Tâches';

CREATE TABLE `plan.milestones`(
  `ID` 						int(16) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Clé primaire',
  `ID_Manager` 				int(8) unsigned DEFAULT NULL 				COMMENT 'Référence créateur',
  `ID_Project` 				int(16) unsigned NOT NULL 					COMMENT 'Référence Projet parent',
  `Label` 					int(32) unsigned NOT NULL 					COMMENT 'Nom',
  `Start_Moment` 			varchar(20) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Timestamp départ',
  `End_Moment` 				varchar(20) COLLATE utf8_bin DEFAULT NULL 	COMMENT 'Timestamp fin',
  `Milestone_Description` 	varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'Description Jalon',
  `Done` 					tinyint(1) DEFAULT NULL 					COMMENT 'Flag archivage',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Manager`,`ID_Project`,`ID_I18n_Label` )
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table des Jalons';

--
-- Tables dédiées a la gestion d'evenements applicatifs
--

CREATE TABLE `ev.states` (
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 			COMMENT 'Clé primaire',
  `ID_App` 			int(4) unsigned DEFAULT NULL 						COMMENT 'Reference Appli',
  `ID_PreAction` 	int(8) unsigned DEFAULT NULL 						COMMENT 'Prédecesseur action,null racine',
  `ID_Target`  		int(8) unsigned NOT NULL 							COMMENT 'Référence Cible',
  `ID_LabelRegroup`	int(8) unsigned DEFAULT NULL 						COMMENT 'Référence (sous groupe) de labels contenant le Titre état',
  `Title_Index`		int(4) unsigned DEFAULT NULL						COMMENT 'Index Titre dans le (sous groupe) de labels',
  `Regroup` 		int(8) unsigned DEFAULT NULL 						COMMENT 'Index de regroupement pour les variations',
  `Cycled` 			tinyint(1) DEFAULT NULL 							COMMENT 'Cyclique',
  `Cycle_ms` 		int(16) unsigned DEFAULT NULL 						COMMENT 'Durée du cycle ms si cyclique',
  `Color` 			varchar(6) COLLATE utf8_bin DEFAULT NULL 			COMMENT 'Couleur Etat',
  `StateType` 		enum('Root','ok','error') COLLATE utf8_bin NOT NULL COMMENT 'Type Etat',
  PRIMARY KEY (`ID`),
  INDEX(`ID_App`,`ID_PreAction`,`ID_Target`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table des Etats possibles';


CREATE TABLE `ev.actions` (
  `ID` 				int(8) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Clé primaire',
  `ID_App` 			int(4) unsigned DEFAULT NULL 				COMMENT 'Reference Appli',
  `ID_PreState` 	int(8) unsigned NOT NULL 					COMMENT 'Prédecesseur Etat',
  `ID_Menu` 		int(8) unsigned DEFAULT NULL 				COMMENT 'Lien Environnement routing parent',
  `ID_Target` 		int(8) unsigned DEFAULT NULL 				COMMENT 'Référence Cible',
  `ID_LabelRegroup`	int(8) unsigned DEFAULT NULL 				COMMENT 'Référence (sous groupe) de labels contenant le Titre action',
  `Title_Index`		int(4) unsigned DEFAULT NULL				COMMENT 'Index Titre dans le (sous groupe) de labels',
  `Cycled` 			tinyint(1) DEFAULT NULL 					COMMENT 'Cyclique',
  `Cycle_ms` 		int(16) unsigned DEFAULT NULL 				COMMENT 'Duree Action ms',
  PRIMARY KEY (`ID`),
  INDEX(`ID_App`,`ID_PreState`,`ID_Menu`,`ID_Target`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table Actions possibles';


--
-- Tables Log
--


CREATE TABLE `debug.user_actions_log` (
  `ID` 				int(48) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Cle primaire',
  `ID_Sysval` 		int(32) unsigned NOT NULL 					COMMENT 'Reference sysval',
  `ID_Action` 		int(8) unsigned NOT NULL 					COMMENT 'Reference Action',
  `ID_Target` 		int(8) unsigned DEFAULT NULL 				COMMENT 'Reference cible',
  `Moment` 			varchar(20) COLLATE utf8_bin NOT NULL 		COMMENT 'Timestamp action',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Sysval`,`ID_Action`,`ID_Target`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table Log des actions utilisateur';


CREATE TABLE `debug.user_errors_log` (
  `ID` 				int(48) unsigned NOT NULL AUTO_INCREMENT 	COMMENT 'Cle primaire',
  `ID_Sysval` 		int(32) unsigned NOT NULL 					COMMENT 'Reference sysval',
  `ID_Error` 		int(8) unsigned NOT NULL 					COMMENT 'Reference Action',
  `ID_Target` 		int(8) unsigned DEFAULT NULL 				COMMENT 'Reference cible',
  `Moment` 			varchar(20) COLLATE utf8_bin NOT NULL 		COMMENT 'Timestamp action',
  PRIMARY KEY (`ID`),
  INDEX(`ID_Sysval`,`ID_Error`,`ID_Target`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table Log des erreurs utilisateur';

